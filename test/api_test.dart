import 'package:flutter_test/flutter_test.dart';
import 'package:hakkanilib/api/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';

void main() {
  test('Api Login Succeeds', () async {
    final loginResponse = await Api.login();

    expect(loginResponse.success, true);
    expect(loginResponse.token, isNotNull);
    expect(loginResponse.user, isNotNull);
  });
  test('Author Fetch Succeeds', () async {
    final authors = await DataEngine.fetcherOf<Author>().fetchMany();

    expect(authors.length, greaterThan(0));
    expect(authors.first.fullName, isNotNull);
  });
  test('Category Fetch Succeeds', () async {
    final categories = await DataEngine.fetcherOf<Category>().fetchMany();

    expect(categories.length, greaterThan(0));
    expect(categories.first.id.value, greaterThan(0));
  });
  test('Book Fetch Succeeds', () async {
    final books = await DataEngine.fetcherOf<Book>().fetchMany();

    expect(books.length, greaterThan(0));
    expect(books.first.category, isNotNull);
    expect(books.first.category.value.id.value, greaterThan(0));
    expect(books.first.author, isNotNull);
    expect(books.first.author.value.id.value, greaterThan(0));
    expect(books.first.poster, isNotNull);
    expect(books.first.poster.id.value, greaterThan(0));
  });
}
