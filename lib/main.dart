import 'package:flutter/widgets.dart';
import 'package:hakkanilib/views/index.dart';
import 'package:hakkanilib/services/startupEngine.dart';

void main() {
  StartupEngine.startup().whenComplete(() => runApp(HomeView()));
}
