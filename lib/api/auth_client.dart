import 'package:hakkanilib/api/response/index.dart';
import 'package:http/http.dart' as http;

class AuthClient extends http.BaseClient {
  final TokenResponse tokenResponse;
  final http.Client _inner = http.Client();

  AuthClient(this.tokenResponse);

  Future<http.StreamedResponse> send(http.BaseRequest request) async {
    if (!tokenResponse.success) {
      throw Exception(
        tokenResponse.exception ?? Exception("failed auth response"),
      );
    }
    request.headers['X-AUTH-TOKEN'] = tokenResponse.token;
    request.persistentConnection = false;
    return await _inner.send(request);
  }
}
