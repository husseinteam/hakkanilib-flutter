import 'package:hakkanilib/api/response/index.dart';

class Credentials extends GenericServer {
  String email;
  String password;

  Credentials({
    this.email,
    this.password,
  });

  @override
  get attributer => (object) {
        this.email = object["email"];
        this.password = object["Password"];
      };

  @override
  Future<Map<String, dynamic>> toMap() async => {
        "email": this.email,
        "password": this.password,
      };
}
