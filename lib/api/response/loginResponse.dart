import 'package:hakkanilib/api/response/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';

class LoginResponse extends TokenResponse {
  User user;

  LoginResponse({
    this.user,
    String token,
    String message,
    bool success,
    Exception exception,
  }) : super(
          token: token,
          message: message,
          success: success,
          exception: exception,
        );

  @override
  Future<Map<String, dynamic>> toMap() async => {
        "user": this.user.toMap(),
        "token": this.token,
        "message": this.message,
        "success": this.success,
      };

  @override
  get attributer => (object) {
        this.user =
            DataEngine.instanceOf(User).fromMap(object["user"]);
        this.token = object["token"];
        this.message = object["message"];
        this.success = object["success"];
      };
}
