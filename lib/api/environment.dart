//import 'package:flutter/foundation.dart';

import 'package:flutter/foundation.dart';

enum Environment {
  localhost,
  jsonRelease,
  production,
}

const Environment defaultEnvironment =
    kReleaseMode ? Environment.production : Environment.localhost;
//Environment.localhost;
