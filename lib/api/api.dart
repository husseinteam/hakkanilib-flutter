import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:hakkanilib/api/index.dart';
import 'package:hakkanilib/api/response/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';

abstract class Api {
  static HttpRequest mainRequest = Requests.main(defaultEnvironment);
  static Future<LoginResponse> login() async {
    if (mainRequest.isJsonRequest) {
      final u = await _parse(json: "assets/json/user.json");
      return LoginResponse(user: DataEngine.instanceOf(User).fromMap(u));
    }
    final current = await mainRequest.call(ApiMethod.httpGet, "users/current");
    return LoginResponse()..ofObject(current);
  }

  static Future<Map<String, dynamic>> _parse({@required String json}) async {
    return rootBundle
        .loadString(json)
        .then((jsonStr) => jsonDecode(jsonStr) as Map<String, dynamic>);
  }
}
