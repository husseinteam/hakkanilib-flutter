import 'package:hakkanilib/api/index.dart';
import 'package:hakkanilib/api/response/index.dart';

class Requests {
  static HttpRequest main(Environment env) {
    final suCredentials =
        Credentials(email: "hussein.sonmez@outlook.com", password: "293117");
    switch (env) {
      case Environment.localhost:
        return HttpRequest(
          // baseUrl: "127.0.0.1:8000",
          baseUrl: "hakkani.site",
          credentials: suCredentials,
        );
      case Environment.jsonRelease:
        return HttpRequest(isJsonRequest: true);
      case Environment.production:
        return HttpRequest(
          baseUrl: "hakkani.site",
          credentials: suCredentials,
        );
      default:
        return null;
    }
  }
}
