import 'dart:convert';
import 'package:hakkanilib/api/index.dart';
import 'package:hakkanilib/api/response/index.dart';
import 'package:http/http.dart' as http;

class HttpRequest {
  HttpRequest({
    this.baseUrl,
    this.credentials,
    this.isJsonRequest: false,
    bool secured: false,
  }) {
    _host = secured ? 'https' : 'http';
  }
  final String baseUrl;
  final Credentials credentials;
  final isJsonRequest;
  TokenResponse _tokenResponse;
  String _host;

  Future _tokenize() async {
    final url = Uri(scheme: _host, host: baseUrl, path: "/auth/login");
    final client = http.Client();
    try {
      final response = await client
          .post(
        url,
        headers: {
          'Content-Type': 'application/json',
        },
        body: await credentials.encodeMap(),
      )
          .catchError((e, st) {
        _tokenResponse = TokenResponse()..exception = e;
      });
      if (response.statusCode == 200) {
        _tokenResponse = TokenResponse()..ofSource(response.body);
      } else {
        try {
          _tokenResponse = TokenResponse()
            ..message = jsonDecode(response.body)["message"].toString();
        } catch (e) {
          _tokenResponse = TokenResponse()..exception = e;
        }
        _tokenResponse.retriedCount++;
      }
    } catch (e) {
      _tokenResponse = TokenResponse()..exception = e;
      _tokenResponse.retriedCount++;
    }
  }

  Future<dynamic> call(
    ApiMethod method,
    String path, {
    Map<String, dynamic> body,
    bool tokenize: true,
    bool responseByteArray: false,
    Map<String, String> params,
  }) async {
    if (tokenize) {
      if (_tokenResponse == null) {
        await _tokenize();
      } else if (_tokenResponse.success == false) {
        if (_tokenResponse.retriedCount < 3) {
          await _tokenize();
        } else {
          throw _tokenResponse.exception;
        }
      }
    }

    Future<http.Response> response;
    final url = Uri(scheme: _host, host: baseUrl, path: path, queryParameters: params);
    // if (params != null && params.length > 0) {
    //   url += "?";
    //   for (var e in params.entries) {
    //     final q = "${e.key}=${e.value}&";
    //     url += q;
    //   }
    //   url = url.substring(0, url.length - 1);
    // }
    final client = tokenize ? AuthClient(_tokenResponse) : http.Client();
    switch (method) {
      case ApiMethod.httpGet:
        response = client.get(url);
        break;
      case ApiMethod.httpPost:
        response = client.post(
          url,
          body: jsonEncode(body),
          headers: {'Content-Type': 'application/json'},
        );
        break;
      case ApiMethod.httpPut:
        response = client.put(url, body: jsonEncode(body));
        break;
      default:
        break;
    }
    final res = await response.catchError((e, st) {
      print(e.toString());
    });
    if (res.statusCode == 503) {
      await Future.delayed(Duration(milliseconds: 200));
      return await this.call(
        method,
        path,
        body: body,
        tokenize: tokenize,
        responseByteArray: responseByteArray,
        params: params,
      );
    } else if (res.statusCode == 403 || res.statusCode == 401) {
      await _tokenize();
      return await this.call(
        method,
        path,
        body: body,
        tokenize: tokenize,
        responseByteArray: responseByteArray,
        params: params,
      );
    } else {
      return responseByteArray ? res.bodyBytes : jsonDecode(res.body);
    }
  }

  Future<dynamic> single(String path) async {
    return await this.call(ApiMethod.httpGet, path);
  }

  Future<Map<String, dynamic>> post(
      String path, Map<String, dynamic> body) async {
    return await this.call(ApiMethod.httpPost, path, body: body)
        as Map<String, dynamic>;
  }

  Future<Map<String, dynamic>> put(
      String path, Map<String, dynamic> body) async {
    return await this.call(ApiMethod.httpPut, path, body: body)
        as Map<String, dynamic>;
  }

  Future<List<TItem>> getList<TItem>(String path) async {
    final iterable =
        (await this.call(ApiMethod.httpGet, path)) as List<dynamic>;
    return iterable.map((item) => item as TItem).toList();
  }
}
