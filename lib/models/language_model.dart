import 'package:flutter/material.dart';
import 'package:hakkanilib/components/forms/options_form.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:get/get.dart';

class LanguageModel implements FormOption {
  @override
  get item => this.appLocale;

  final AppLocale appLocale;
  static final appLocaleOptions =
      AppLocales.keys.map((l) => LanguageModel(l)).toList();

  LanguageModel(this.appLocale);

  @override
  Widget get subtitle => Text(
        "title.change_system_language".trArgs([this.appLocale.lang.tr]),
      );

  @override
  Widget get title => Text(this.appLocale.lang.tr);
}
