import 'package:flutter/material.dart';
import 'package:hakkanilib/components/forms/options_form.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:get/get.dart';

class CategoryModel implements FormOption {
  @override
  get item => this.category;

  final Category category;
  CategoryModel(this.category);

  @override
  Widget get subtitle => Text(
        "title.option_category_subtitle".trArgs([this.category.title.string]),
      );

  @override
  Widget get title => Text(this.category.title.string);
}
