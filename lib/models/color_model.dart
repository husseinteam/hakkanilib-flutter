import 'package:flutter/material.dart';
import 'package:hakkanilib/components/forms/options_form.dart';
import 'package:get/get.dart';

class ColorModel implements FormOption {
  final Color color;

  @override
  get item => this.color;

  static var models = [
    Colors.orange[500],
    Colors.blue,
    Colors.cyan,
    Colors.deepOrange,
    Colors.deepOrangeAccent,
    Colors.deepPurpleAccent,
    Colors.green,
    Colors.lightBlue,
    Colors.teal
  ].map((c) => ColorModel(c)).toList();

  ColorModel(this.color);

  @override
  Widget get subtitle => Text("title.change_color".tr);

  @override
  Widget get title => Text(
        "title.dhikr_color_will_be_changed_to".tr,
        style: TextStyle(color: color),
      );
}
