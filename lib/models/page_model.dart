import 'package:flutter/material.dart';
import 'package:hakkanilib/components/forms/options_form.dart';
import 'package:get/get.dart';

class PageModel implements FormOption {
  @override
  get item => this.page;

  final int page;
  PageModel(this.page);

  static Iterable<PageModel> options(int pageCount) sync* {
    for (var i in Iterable<int>.generate(pageCount)) {
      yield PageModel(i + 1);
    }
  }

  @override
  Widget get subtitle => Text("title.option_page_will_be_selected".tr);

  @override
  Widget get title =>
      Text("title.option_page_to_go".trArgs([this.page.toString()]));
}
