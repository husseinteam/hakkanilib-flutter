import 'package:flutter/material.dart';
import 'package:hakkanilib/components/forms/options_form.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:get/get.dart';

class AuthorModel implements FormOption {
  @override
  get item => this.author;

  final Author author;
  AuthorModel(this.author);

  @override
  Widget get subtitle => Text("title.option_author_subtitle".tr);

  @override
  Widget get title => Text(this.author.fullName.string);
}
