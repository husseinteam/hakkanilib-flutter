import 'dart:io';
import 'package:flutter/foundation.dart' show required, kReleaseMode;
import 'package:hakkanilib/data/metadata/index.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/repo/index.dart';

typedef GeneratorDelegate = GenericEntity Function();

class DataEngine {
  static final List<GeneratorDelegate> _generators = <GeneratorDelegate>[
    () => Category(),
    () => Author(),
    () => Book(),
    () => Poster(),
    () => AppSettings(),
    () => User(),
  ];

  static Database _db;
  static Database _prevDB;
  static const _version = 4;
  static Database get db => _prevDB ?? _db;

  static Future<void> init() async {
    var databaseDirectory = await getDatabasesPath();
    final dbFile = File(join(databaseDirectory, "app.db"));
    if (_db == null) {
      _db = await openDatabase(
        dbFile.path,
        version: _version,
        onUpgrade: (db, prev, next) async {
          await regenerateDB(db, upgrade: true);
        },
        onOpen: (db) async {
          if (!kReleaseMode) {
            // await regenerateDB(db);
          }
        },
        onCreate: (Database db, int version) async {
          await regenerateDB(db);
        },
      );
    }
  }

  static Future<void> pragmaForeignKeys({@required bool on}) async {
    final word = on ? 'ON' : 'OFF';
    await (db).execute('PRAGMA foreign_keys = $word;');
  }

  static Future<void> regenerateDB(Database _db, {bool upgrade: false}) async {
    _prevDB = _db;
    for (var gen in _generators) {
      final e = gen();
      await pragmaForeignKeys(on: false);
      if (upgrade == false) {
        await db.execute("DROP TABLE IF EXISTS ${e.tableInfo.tableName};");
      }
      await pragmaForeignKeys(on: true);
      await db.execute('${e.tableInfo}');
      if (upgrade && e.tableInfo.altered) {
        for (FieldInfo newField
            in e.tableInfo.fields.where((fi) => fi.newVersion)) {
          final liRes = await db.rawQuery(
              "SELECT 'ALTER TABLE ${e.tableInfo.tableName} ADD $newField' AS ALT FROM sqlite_master "
              "WHERE type = 'table' AND name = '${e.tableInfo.tableName}' AND sql NOT LIKE ?",
              ["'%${newField.prop.name}%'"]);
          if (liRes.length > 0) {
            final String alterStatement = liRes.first['ALT'];
            await db.execute(alterStatement).catchError(
                  (e, st) => print(e.toString()),
                );
          }
        }
        for (SingleReferenceInfo sr in (e.tableInfo.singleReferences ?? [])) {
          try {
            await db.execute(sr.alterStatement());
          } on Exception catch (_) {
            print(_.toString());
          }
        }
      }
    }
    _prevDB = null;
  }

  static GenericEntity instanceOf(Type t) {
    return _generators.firstWhere((e) => e().runtimeType == t, orElse: null)();
  }

  static Repository<T> repositoryOf<T extends GenericEntity<T>>() {
    return Repository(() => instanceOf(T));
  }

  static Fetcher<T> fetcherOf<T extends GenericEntity<T>>() {
    return Fetcher(() => instanceOf(T));
  }
}
