import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/metadata/index.dart';
import 'package:hakkanilib/services/index.dart';

class Repository<T extends GenericEntity<T>> {
  final T Function() entityInstance;

  Repository(this.entityInstance);

  Future<List<T>> all({
    String whereString,
    List<dynamic> args,
    bool digin: true,
  }) async {
    var t = entityInstance();
    var map = await DataEngine.db.transaction(
      (txn) async => await txn.query(
        t.tableInfo.tableName,
        columns: t.tableInfo.nonBlobCoumns,
        where: whereString,
        whereArgs: args,
      ),
    );
    List<T> list = [];
    if (map.isNotEmpty) {
      for (var itemMap in map) {
        t = t.fromMap(itemMap);
        final tId = t.id.value;
        if (digin) {
          for (var collInfo in t.tableInfo.collectionInfos ?? []) {
            List<dynamic> items;
            final TableInfo itemTableInfo =
                collInfo.collectionProp.itemRepo.entityInstance().tableInfo;
            final ReferenceInfo itemReferenceInfo =
                itemTableInfo.referenceInfos.firstWhere(
              (refInfo) =>
                  refInfo.referenceRepo.entityInstance().tableInfo.tableName ==
                  t.tableInfo.tableName,
              orElse: () => null,
            );
            final whereString = "${itemReferenceInfo.referencingColumn} = ?";
            items = await collInfo.collectionProp.itemRepo.all(
              whereString: whereString,
              args: [tId],
            );
            items.forEach((it) => it.owner = t);
            collInfo.collectionProp.setter(t, items);
          }
        }
        for (var ri in t.tableInfo.referenceInfos ?? []) {
          final int refId = ri.idProp.getter(t);
          if (ri.prop != null) {
            final refEntity = await ri.referenceRepo.single(refId);
            ri.prop.setter(t, refEntity);
          }
        }
        for (var singleRef in t.tableInfo.singleReferences ?? []) {
          final sid = itemMap[singleRef.idProp.name];
          singleRef.idProp.setter(t, sid);
          final e = await singleRef.referenceRepository.single(sid);
          singleRef.prop.setter(t, e);
        }
        await t.singleModifier((refInfo, referenceEntity) async {
          refInfo.prop.setter(t, referenceEntity);
        });
        list.add(t);
      }
    }
    return list;
  }

  Future<T> first({
    String whereString,
    List<dynamic> args,
  }) async {
    final list = await all(whereString: whereString, args: args);
    if (list.length > 0) {
      return list.first;
    } else {
      return null;
    }
  }

  Future<T> single(int id) async {
    if (id != null) {
      final list = await all(whereString: 'id = ?', args: [id]);
      return list.isNotEmpty ? list.first : null;
    } else {
      return null;
    }
  }

  Future<int> blobLength(T entity, FieldInfo<T> blobField) async {
    final int blobLength = (await DataEngine.db.rawQuery(
            'SELECT length(${blobField.prop.name}) as LEN FROM ${entity.tableInfo.tableName} WHERE id = ${entity.id}'))
        .first["LEN"];
    return blobLength ?? 0;
  }

  Future<void> processBlob({
    @required T of,
    List<FieldInfo<T>> processFields,
  }) async {
    for (var bfi in processFields) {
      assert(bfi.dataType == DataType.blob);
      if (bfi.prop.getter(of) != null) {
        continue;
      }
      final _blobLen = await blobLength(of, bfi);
      if (_blobLen > 0) {
        var blobVal = Uint8List(_blobLen);
        final chunkLength = 1048576;
        var c = 1;
        do {
          final List<int> blobChunk = (await DataEngine.db.rawQuery(
                  'SELECT substr(${bfi.prop.name}, $c, $chunkLength) as STR FROM ${of.tableInfo.tableName} WHERE id = ${of.id}'))
              .first["STR"];
          blobVal.setRange(c - 1, c + blobChunk.length - 1, blobChunk);
          c += chunkLength;
        } while (c < _blobLen);
        bfi.prop.setter(of, blobVal);
      }
    }
  }

  Future<int> count() async {
    final map = await DataEngine.db.transaction(
      (txn) async => await txn.rawQuery(
          "SELECT COUNT(*) AS C FROM ${entityInstance().tableInfo.tableName}"),
    );
    return int.parse(map.first["C"].toString());
  }

  Future<T> insert(T entity, {bool digin: true}) async {
    final existing = await single(entity.id.value);
    if (existing != null) {
      return existing;
    }
    for (var ri in entity.tableInfo.referenceInfos ?? []) {
      if (ri.prop != null) {
        final refEntity = ri.prop.getter(entity);
        if (refEntity != null && refEntity.id.value != 0) {
          ri.prop.setter(
            entity,
            await ri.referenceRepo.insert(refEntity, digin: false),
          );
        }
      }
    }
    await entity.singleModifier((refInfo, referenceEntity) async {
      final e = await refInfo.referenceRepository
          .insert(referenceEntity, digin: false);
      refInfo.prop.setter(entity, e);
      refInfo.idProp.setter(entity, e.id.value);
    });
    entity.id.value = await DataEngine.db.transaction(
      (txn) async =>
          await txn.insert(entity.tableInfo.tableName, entity.toMap()),
    );
    if (digin) {
      for (CollectionInfo<T> collInfo
          in entity.tableInfo.collectionInfos ?? []) {
        var items = collInfo.collectionProp.getter(entity);
        for (var item in items) {
          await collInfo.collectionProp.itemRepo.insert(item..owner = entity);
        }
      }
    }
    return entity;
  }

  Future<T> update(T entity, {bool digin: false}) async {
    await entity.singleModifier((refInfo, referenceEntity) async {
      final e = await refInfo.referenceRepository
          .upsert(referenceEntity, digin: digin);
      refInfo.prop.setter(entity, e);
    });
    var map = entity.toMap(update: true);
    await DataEngine.db.transaction(
      (txn) async => await txn.update(
        entity.tableInfo.tableName,
        map,
        where: "id = ?",
        whereArgs: [entity.id.value],
      ),
    );
    if (digin) {
      for (CollectionInfo<T> collInfo
          in entity.tableInfo.collectionInfos ?? []) {
        var items = collInfo.collectionProp.getter(entity);
        for (var item in items) {
          item.owner = entity;
          await collInfo.collectionProp.itemRepo.update(item, digin: digin);
        }
        collInfo.collectionProp.setter(entity, items);
      }
    }
    return entity;
  }

  Future<int> updateColumns(T entity, List<String> columns) async {
    final entries = entity.toMapEntries(columns..add('id'));
    final c = await DataEngine.db.transaction(
      (txn) async => await txn.update(
        entity.tableInfo.tableName,
        Map<String, dynamic>.fromEntries(entries),
        where: "id = ?",
        whereArgs: [entity.id.value],
      ),
    );
    return c;
  }

  Future<int> delete(T entity) async {
    var deletedCount = 0;

    for (var collInfo in entity.tableInfo.collectionInfos ?? []) {
      final colp = collInfo.collectionProp;
      var items = await colp.itemRepo.all(
          whereString: "${collInfo.referenceName} = ?",
          args: [entity.id.value]);
      for (var item in items) {
        deletedCount += await colp.itemRepo.delete(item);
      }
    }

    deletedCount += await DataEngine.db.transaction(
      (txn) async => await txn.delete(
        entity.tableInfo.tableName,
        where: "id = ?",
        whereArgs: [entity.id.value],
      ),
    );

    return deletedCount;
  }

  Future<int> deleteAll({TableInfo ti}) async {
    int total = 0;
    final _ti = ti ?? (entityInstance().tableInfo);
    var childCount = (_ti.collectionInfos ?? []).length;
    if (childCount > 0) {
      for (var ci in _ti.collectionInfos) {
        final childTi = ci.collectionProp.itemRepo.entityInstance().tableInfo;
        if (ci.collectionProp.loop == false) {
          total += await deleteAll(ti: childTi);
        } else {
          (await ci.collectionProp.itemRepo.all()).forEach(
            (it) async => ci.collectionProp.itemRepo.delete(it),
          );
        }
      }
    }
    await DataEngine.pragmaForeignKeys(on: false);
    for (var singleRef
        in (_ti.singleReferences ?? []).where((si) => !si.volatile)) {
      final childTi = singleRef.referenceRepository.entityInstance().tableInfo;
      total += await deleteAll(ti: childTi);
    }
    total += await DataEngine.db.transaction(
      (txn) async {
        int c = await txn.rawDelete("DELETE from ${_ti.tableName}");
        return c;
      },
    );
    await DataEngine.pragmaForeignKeys(on: true);
    return total;
  }

  Future<T> upsert(T entity, {bool digin: false}) async {
    final existing = await single(entity.id.value);
    if (existing == null) {
      return await this.insert(entity, digin: digin);
    } else {
      entity.setDifferences(withEntity: existing);
      if (entity.differs(withFields: ['updated_at'])) {
        return await this.update(entity, digin: digin);
      } else {
        return existing;
      }
    }
  }
}
