import 'package:hakkanilib/api/index.dart';
import 'package:hakkanilib/data/entities/index.dart';

class Fetcher<T extends GenericEntity<T>> {
  final T Function() _tGenerator;
  Fetcher(this._tGenerator);

  Future<List<T>> fetchMany() async {
    final response = await Api.mainRequest
        .single("${_tGenerator().tableInfo.resource}/list");
    final List<T> listOfT = [];
    if (response['error'] == false) {
      for (var map in response['list']) {
        final item = _tGenerator().fromMap(map);
        listOfT.add(item);
      }
    }
    return listOfT;
  }

  Future<T> fetchSingle(int id) async {
    final t = _tGenerator();
    final response =
        await Api.mainRequest.single("${t.tableInfo.resource}/single/$id");
    return response['error'] == false ? t.fromMap(response['single']) : null;
  }

  Future<T> fetchHttpPost<T>(
    String path,
    Map<String, dynamic> body, {
    bool responseByteArray: false,
  }) async {
    final response = await Api.mainRequest.call(
      ApiMethod.httpPost,
      path,
      body: body,
      responseByteArray: responseByteArray,
    );
    return response as T;
  }
}
