import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/metadata/index.dart';

class Category extends GenericEntity<Category> {
  var sortMode = false.obs;

  var title = "".obs;
  var parentId = RxInt(null);

  Category parent;

  @override
  FieldInfo<Category> get uniqueField => fieldInfo(withName: "title");

  @override
  String toString() => title.value;

  @override
  TableInfo<Category> get tableInfo => TableInfo<Category>(
        tableName: "categories",
        resource: "categories",
        translation: "entity.book",
        fieldInfos: [
          FieldInfo(
            translation: "entity.book.title",
            prop: Prop(
              name: "title",
              getter: (e) => e.title.value,
              setter: (e, val) => e.title.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.book.parent_id",
            selfReference: true,
            dataType: DataType.int,
            prop: Prop(
              name: "parent_id",
              getter: (e) => e.parentId.value,
              setter: (e, val) => e.parentId.value = val,
            ),
          ),
        ],
      );

  @override
  int get hashCode => title.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is Category && title == other.title;
}
