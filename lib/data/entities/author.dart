import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/metadata/index.dart';

class Author extends GenericEntity<Author> {
  var sortMode = false.obs;

  var fullName = "".obs;

  @override
  FieldInfo<Author> get uniqueField => fieldInfo(withName: "full_name");

  @override
  String toString() => fullName.value;

  @override
  TableInfo<Author> get tableInfo => TableInfo<Author>(
        tableName: "authors",
        resource: "authors",
        translation: "entity.book",
        fieldInfos: [
          FieldInfo(
            translation: "entity.book.fullName",
            prop: Prop(
              name: "full_name",
              getter: (e) => e.fullName.value,
              setter: (e, val) => e.fullName.value = val,
            ),
          ),
        ],
      );

  @override
  int get hashCode => fullName.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is Author && fullName == other.fullName;
}
