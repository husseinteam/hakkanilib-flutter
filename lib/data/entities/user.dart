import 'dart:ui';

import 'package:get/get.dart';
import 'package:hakkanilib/data/metadata/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/api/environment.dart';
import 'package:hakkanilib/utils/index.dart';

class User extends GenericEntity<User> {
  static const TABLE_NAME = "users";

  var sortMode = false.obs;
  var loaded = (defaultEnvironment == Environment.jsonRelease).obs;

  var email = "".obs;
  var identity = "".obs;
  var initials = "".obs;
  var apiToken = "".obs;
  var password = "".obs;

  var appSettingsId = 0.obs;
  var appSettings = AppSettings().obs;

  Future<void> updateLocale(Locale appLocale) async {
    appSettings.value.language.value = appLocale.toString();
    await DataEngine.repositoryOf<AppSettings>()
        .updateColumns(appSettings.value, ['language']);
    Get.updateLocale(appLocale);
  }

  Locale getLocale() {
    final al = appSettings.value.language.value.appLocale;
    return AppLocales[al];
  }

  @override
  String toString() => initials.value;

  @override
  TableInfo<User> get tableInfo => TableInfo<User>(
        tableName: User.TABLE_NAME,
        resource: "user",
        translation: "entity.user",
        fieldInfos: [
          FieldInfo(
            translation: "entity.user.email",
            prop: Prop(
              name: "email",
              getter: (e) => e.email.value,
              setter: (e, val) => e.email.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.user.identity",
            prop: Prop(
              name: "identity",
              getter: (e) => e.identity.value,
              setter: (e, val) => e.identity.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.user.initials",
            prop: Prop(
              name: "initials",
              getter: (e) => e.initials.value,
              setter: (e, val) => e.initials.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.user.apiToken",
            prop: Prop(
              name: "apiToken",
              getter: (e) => e.apiToken.value,
              setter: (e, val) => e.apiToken.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.user.password",
            prop: Prop(
              name: "password",
              getter: (e) => e.password.value,
              setter: (e, val) => e.password.value = val,
            ),
          ),
        ],
        singleReferences: [
          SingleReferenceInfo(
            idProp: Prop<User, int>(
              name: "app_settings_id",
              getter: (e) => e.appSettingsId.value,
              setter: (e, val) => e.appSettingsId.value = val,
            ),
            prop: Prop(
              name: "appSettings",
              getter: (e) => e.appSettings.value,
              setter: (e, val) => e.appSettings.value = val,
            ),
            referenceRepository: DataEngine.repositoryOf<AppSettings>(),
            foreignTable: AppSettings.TABLE_NAME,
          ),
        ],
      );

  @override
  int get hashCode =>
      email.hashCode ^
      identity.hashCode ^
      apiToken.hashCode ^
      password.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is User &&
          email == other.email &&
          identity == other.identity &&
          initials == other.initials &&
          apiToken == other.apiToken &&
          password == other.password
      // && listEquals(dhikrGroups, other.dhikrGroups)
      ;
}
