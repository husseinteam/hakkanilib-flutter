import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/foundation.dart' hide Category;
import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/metadata/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/controllers/index.dart';

class Book extends GenericEntity<Book> {
  static const String TABLE_NAME = "books";

  var sortMode = false.obs;

  var pdfName = "".obs;
  var pdfOriginalName = "".obs;
  var pdfSize = 0.obs;
  var title = "".obs;
  var rtl = 0.obs;
  var published = 0.obs;
  var nextBookId = 0.obs;
  var lastPageOrder = 1.obs;
  var isFavorite = 0.obs;
  var categoryId = 0.obs;
  var authorId = 0.obs;
  var posterId = 0.obs;
  var posterData = Rx<Uint8List>(null);
  var bookData = Rx<Uint8List>(null);
  var category = Rx<Category>(null);
  var author = Rx<Author>(null);
  Poster poster;
  Book nextBook;

  @override
  FieldInfo<Book> get uniqueField => fieldInfo(withName: "title");

  File get posterFile => Book.localFile(withName: poster.posterName.value);

  static File localFile({@required String withName, String appPath}) {
    final path = appPath ?? Get.find<BooksController>().appPath;
    return File('$path/$withName');
  }

  @override
  String toString() => title.value;

  @override
  TableInfo<Book> get tableInfo => TableInfo<Book>(
        tableName: Book.TABLE_NAME,
        resource: "books",
        translation: "entity.book",
        fieldInfos: [
          FieldInfo(
            translation: "entity.book.pdfName",
            nullableOnForm: true,
            prop: Prop(
              name: "pdfName",
              getter: (e) => e.pdfName.value,
              setter: (e, val) => e.pdfName.value = val,
              defaultValueGetter: () => "",
            ),
          ),
          FieldInfo(
            translation: "entity.book.pdfOriginalName",
            nullableOnForm: true,
            prop: Prop(
              name: "pdfOriginalName",
              getter: (e) => e.pdfOriginalName.value,
              setter: (e, val) => e.pdfOriginalName.value = val,
              defaultValueGetter: () => "",
            ),
          ),
          FieldInfo(
            translation: "entity.book.pdfSize",
            dataType: DataType.int,
            nullableOnForm: true,
            prop: Prop(
              name: "pdfSize",
              getter: (e) => e.pdfSize.value,
              setter: (e, val) => e.pdfSize.value = val,
              defaultValueGetter: () => 0,
            ),
          ),
          FieldInfo(
            translation: "entity.book.title",
            prop: Prop(
              name: "title",
              getter: (e) => e.title.value,
              setter: (e, val) => e.title.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.book.rtl",
            dataType: DataType.int,
            newVersion: true,
            prop: Prop(
              name: "rtl",
              getter: (e) => e.rtl.value,
              setter: (e, val) =>
                  e.rtl.value = val is bool ? (val == true ? 1 : 0) : val,
            ),
          ),
          FieldInfo(
            translation: "entity.book.published",
            dataType: DataType.int,
            newVersion: true,
            prop: Prop(
              name: "published",
              getter: (e) => e.published.value,
              setter: (e, val) =>
                  e.published.value = val is bool ? (val == true ? 1 : 0) : val,
            ),
          ),
          FieldInfo(
            translation: "entity.book.lastPageOrder",
            dataType: DataType.int,
            local: true,
            prop: Prop(
              name: "lastPageOrder",
              getter: (e) => e.lastPageOrder.value,
              setter: (e, val) => e.lastPageOrder.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.book.isFavorite",
            dataType: DataType.int,
            local: true,
            prop: Prop(
              name: "isFavorite",
              getter: (e) => e.isFavorite.value,
              setter: (e, val) => e.isFavorite.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.book.posterData",
            dataType: DataType.blob,
            local: true,
            prop: Prop(
              name: "posterData",
              getter: (e) => e.posterData.value,
              setter: (e, val) => e.posterData.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.book.bookData",
            dataType: DataType.blob,
            local: true,
            prop: Prop(
              name: "bookData",
              getter: (e) => e.bookData.value,
              setter: (e, val) => e.bookData.value = val,
            ),
          ),
        ],
        referenceInfos: [
          ReferenceInfo(
            idProp: Prop<Book, int>(
              name: "category_id",
              getter: (e) => e.categoryId.value,
              setter: (e, val) => e.categoryId.value = val,
            ),
            prop: Prop<Book, Category>(
              name: "category",
              getter: (e) => e.category.value,
              setter: (e, val) => e.category.value = val,
            ),
            referenceRepo: DataEngine.repositoryOf<Category>(),
            referencingColumn: "category_id",
          ),
          ReferenceInfo(
            idProp: Prop<Book, int>(
              name: "author_id",
              getter: (e) => e.authorId.value,
              setter: (e, val) => e.authorId.value = val,
            ),
            prop: Prop<Book, Author>(
              name: "author",
              getter: (e) => e.author.value,
              setter: (e, val) => e.author.value = val,
            ),
            referenceRepo: DataEngine.repositoryOf<Author>(),
            referencingColumn: "author_id",
          ),
        ],
        singleReferences: [
          SingleReferenceInfo(
            prop: Prop(
              name: "poster",
              getter: (e) => e.poster,
              setter: (e, val) => e.poster = val,
            ),
            idProp: Prop<Book, int>(
              name: "poster_id",
              getter: (e) => e.posterId.value,
              setter: (e, val) => e.posterId.value = val,
            ),
            referenceRepository: DataEngine.repositoryOf<Poster>(),
            foreignTable: Poster.TABLE_NAME,
          ),
          SingleReferenceInfo(
            prop: Prop(
              name: "next",
              getter: (e) => e.nextBook,
              setter: (e, val) => e.nextBook = val,
            ),
            idProp: Prop<Book, int>(
              name: "next_id",
              getter: (e) => e.nextBookId.value,
              setter: (e, val) => e.nextBookId.value = val,
            ),
            referenceRepository: DataEngine.repositoryOf<Book>(),
            foreignTable: Book.TABLE_NAME,
            volatile: true,
          ),
        ],
      );

  @override
  int get hashCode =>
      pdfName.hashCode ^
      pdfOriginalName.hashCode ^
      pdfSize.hashCode ^
      title.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Book &&
          pdfName == other.pdfName &&
          pdfOriginalName == other.pdfOriginalName &&
          pdfSize == other.pdfSize &&
          title == other.title &&
          category == other.category &&
          poster == other.poster &&
          nextBook == other.nextBook;
}
