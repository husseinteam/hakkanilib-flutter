import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/metadata/index.dart';

class Poster extends GenericEntity<Poster> {
  static const TABLE_NAME = 'posters';
  var sortMode = false.obs;

  var posterName = "".obs;
  var posterOriginalName = "".obs;
  var posterSize = 0.obs;

  @override
  FieldInfo<Poster> get uniqueField => fieldInfo(withName: "posterName");

  @override
  String toString() => posterName.value;

  @override
  TableInfo<Poster> get tableInfo => TableInfo<Poster>(
        tableName: Poster.TABLE_NAME,
        resource: "posters",
        translation: "entity.book",
        fieldInfos: [
          FieldInfo(
            translation: "entity.book.posterName",
            nullableOnForm: true,
            prop: Prop(
              name: "posterName",
              getter: (e) => e.posterName.value,
              setter: (e, val) => e.posterName.value = val,
              defaultValueGetter: () => "",
            ),
          ),
          FieldInfo(
            translation: "entity.book.posterOriginalName",
            nullableOnForm: true,
            prop: Prop(
              name: "posterOriginalName",
              getter: (e) => e.posterOriginalName.value,
              setter: (e, val) => e.posterOriginalName.value = val,
              defaultValueGetter: () => "",
            ),
          ),
          FieldInfo(
            translation: "entity.book.posterSize",
            dataType: DataType.int,
            nullableOnForm: true,
            prop: Prop(
              name: "posterSize",
              getter: (e) => e.posterSize.value,
              setter: (e, val) => e.posterSize.value = val,
              defaultValueGetter: () => 0,
            ),
          ),
        ],
      );

  @override
  int get hashCode =>
      posterName.hashCode ^ posterOriginalName.hashCode ^ posterSize.hashCode;
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Poster &&
          posterName == other.posterName &&
          posterOriginalName == other.posterOriginalName &&
          posterSize == other.posterSize;
}
