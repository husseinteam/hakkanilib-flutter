import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hakkanilib/data/metadata/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/utils/index.dart';

typedef Future<void> SingleModifierCallback(
    dynamic refInfo, GenericEntity referenceEntity);

abstract class GenericEntity<TEntity extends GenericEntity<TEntity>>
    extends GetxController {
  RxInt id = 0.obs;
  var insertedAt = "".obs;
  var updatedAt = "".obs;

  var differences = <String>[];

  TableInfo<TEntity> get tableInfo;
  FieldInfo<TEntity> get uniqueField => fieldInfo(withName: "id");
  dynamic get uniqueValue => uniqueField.prop.getter(this as TEntity);
  GenericEntity owner;

  void reset() {
    for (var fi in tableInfo.fieldInfos.where((e) => e.resetTo != null)) {
      fi.prop.setter(this, fi.resetTo);
    }
  }

  FieldInfo<TEntity> fieldInfo({@required String withName}) => tableInfo.fields
      .firstWhere((fi) => fi.prop.name == withName, orElse: () => null);

  void setDifferences({@required TEntity withEntity}) {
    differences.clear();
    for (var fi in this.tableInfo.fields) {
      if (fi.prop.getter(this) != fi.prop.getter(withEntity) && !fi.local) {
        differences.add(fi.prop.name);
      }
    }
  }

  bool differs(
      {@required List<String> withFields, bool fixDifferences: false}) {
    final result = differences.any((d) => withFields.contains(d));
    if (fixDifferences) {
      differences.removeWhere((d) => withFields.contains(d));
    }
    return result;
  }

  bool isSameServerTime({@required TEntity by}) {
    if (this.updatedAt.string.isNotEmpty) {
      final otherDate = by.updatedAt.string.isNotEmpty
          ? by.updatedAt.value.toDateTime()
          : DateTime.now();
      return this.updatedAt.value.toDateTime().isSameDate(otherDate);
    } else {
      return false;
    }
  }

  void copy({TEntity from, bool includeSingleRefs: true}) {
    for (var fi in tableInfo.fields) {
      final val = fi.prop.getter(from);
      if ((val is int && val != 0) || (val is String && val.isNotEmpty)) {
        fi.prop.setter(this, val);
      }
    }
    final singleRefCount = (tableInfo.singleReferences ?? []).length;
    if (singleRefCount > 0 && includeSingleRefs) {
      for (var tO2ORef in tableInfo.singleReferences) {
        final val = tO2ORef.prop.getter(from);
        tO2ORef.prop.setter(this, val);
      }
    }
  }

  TEntity fromMap(Map<String, dynamic> map,
      {GenericEntity withOwner, bool omitId = false}) {
    TEntity t = DataEngine.instanceOf(TEntity);
    t.owner = withOwner ?? owner;
    if (omitId == false) {
      t.id.value = map["id"];
    }
    tableInfo.fields
        .where((fi) => !omitId || fi.prop.name != "id")
        .forEach((fi) {
      if (fi.prop.defaultValueGetter != null && map[fi.prop.name] == null) {
        fi.prop.setter(t, fi.prop.defaultValueGetter());
      } else if (map.keys.contains(fi.prop.name)) {
        fi.prop.setter(t, map[fi.prop.name]);
      }
    });
    for (var ri in tableInfo.referenceInfos ?? []) {
      if (ri.prop != null && map.keys.contains(ri.prop.name)) {
        final reference = ri.referenceRepo.entityInstance().fromMap(
              map[ri.prop.name],
              withOwner: this,
              omitId: omitId,
            );
        ri.prop.setter(t, reference);
      }
      if (ri.idProp != null && map.keys.contains(ri.idProp.name)) {
        ri.idProp.setter(t, map[ri.idProp.name]);
      }
    }
    for (var singleInfo in tableInfo.singleReferences ?? []) {
      if (map[singleInfo.prop.name] != null) {
        final refEntity =
            singleInfo.referenceRepository.entityInstance().fromMap(
                  map[singleInfo.prop.name],
                  withOwner: this,
                  omitId: omitId,
                );
        singleInfo.prop.setter(t, refEntity);
        singleInfo.idProp.setter(t, refEntity.id.value);
      }
    }
    if (tableInfo.collectionInfos != null) {
      tableInfo.collectionInfos.where((e) => map[e.name] != null).forEach(
          (ci) => ci.collectionProp.setter(
              t,
              map[ci.name]
                  .map((itemMap) => ci.collectionProp.itemRepo
                      .entityInstance()
                      .fromMap(itemMap, withOwner: this, omitId: omitId))
                  .toList()));
    }
    return t;
  }

  Map<String, dynamic> toMap({bool update: false}) {
    var map = Map<String, dynamic>();
    for (var fi in tableInfo.fields) {
      if (update) {
        if (!fi.local) {
          map[fi.prop.name] = fi.prop.getter(this as TEntity);
        }
        if (map[fi.prop.name] == null) {
          map.remove(fi.prop.name);
        }
      } else {
        map[fi.prop.name] =
            fi.prop.defaultValueGetter != null && fi.nullableOnForm == false
                ? fi.prop.defaultValueGetter()
                : fi.prop.getter(this as TEntity);
        if (fi.prop.defaultValueGetter != null) {
          fi.prop.setter(this as TEntity, map[fi.prop.name]);
        }
      }
    }
    for (var ri in tableInfo.referenceInfos ?? []) {
      if (ri.prop != null) {
        final getter = ri.getForeignId ?? (e) => e != null ? e.id.value : null;
        final riEntity = ri.prop.getter(this);
        final refIdValue = riEntity?.id?.value;
        map[ri.idProp.name] = (refIdValue != null && refIdValue != 0)
            ? refIdValue
            : getter(this.owner);
        if (map[ri.idProp.name] == null) {
          map.remove(ri.idProp.name);
        } else {
          ri.idProp.setter(this, map[ri.idProp.name]);
        }
      } else {
        final getter = ri.getForeignId ?? (e) => e != null ? e.id.value : null;
        final fieldIsSelfReference =
            fieldInfo(withName: ri.referencingColumn).selfReference;
        map[ri.referencingColumn] = getter(fieldIsSelfReference ? this : owner);
        if (map[ri.referencingColumn] == null) {
          map.remove(ri.referencingColumn);
        }
      }
    }
    for (var singleInfo in tableInfo.singleReferences ?? []) {
      if (singleInfo.idProp != null) {
        if (singleInfo.idProp.getter(this) != 0) {
          map[singleInfo.idProp.name] = singleInfo.idProp.getter(this);
        } else if (singleInfo.prop.getter(this) != null) {
          map[singleInfo.idProp.name] = singleInfo.prop.getter(this).id.value;
        } else {
          map[singleInfo.idProp.name] = null;
        }
      }
    }
    return map;
  }

  Future<void> fixReferences() async {
    for (var ri in tableInfo.referenceInfos ?? []) {
      if (ri.prop != null) {
        final refIdValue = ri.idProp.getter(this);
        final riEntity = await ri.referenceRepo.single(refIdValue);
        if (riEntity.id.value != 0) {
          ri.prop.setter(this, riEntity);
        }
      } else {
        final getter = ri.getForeignId ?? (e) => e != null ? e.id.value : null;
        final fieldIsSelfReference =
            fieldInfo(withName: ri.referencingColumn).selfReference;
        final refId = getter(fieldIsSelfReference ? this : owner);
        if (refId != null) {
          ri.idProp.setter(this, refId);
        }
      }
    }
  }

  List<MapEntry<String, dynamic>> toMapEntries(List<String> columns) {
    final Prop<TEntity, dynamic> Function(String) getProp = (String name) {
      return fieldInfo(withName: name)?.prop ??
          tableInfo.singleReferences
              .firstWhere((sr) => sr.idProp.name == name)
              .idProp;
    };
    return columns
        .map((colName) => MapEntry<String, dynamic>(
              colName,
              getProp(colName).getter(this as TEntity),
            ))
        .toList();
  }

  Future<void> singleModifier(SingleModifierCallback modifier) async {
    if ((tableInfo.singleReferences ?? []).length == 0) {
      return;
    }
    for (var singleRef
        in tableInfo.singleReferences.where((sr) => !sr.volatile)) {
      final referenceRepo = singleRef.referenceRepository;
      final referencingId = singleRef.idProp.getter(this);
      final referenceEntity = await referenceRepo.single(referencingId);
      if ((referenceEntity ?? singleRef.prop.getter(this)) != null) {
        await modifier(
            singleRef, referenceEntity ?? singleRef.prop.getter(this));
      }
    }
  }
}
