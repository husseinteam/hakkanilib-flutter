import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/metadata/index.dart';
import 'package:hakkanilib/data/repo/dataEngine.dart';
import 'package:hakkanilib/services/index.dart';

class AppSettings extends GenericEntity<AppSettings> {
  static const TABLE_NAME = "app_settings";

  var purchased = 0.obs;
  var rewardDate = "".obs;
  var vibrationOff = 0.obs;
  var dhikrColor = "".obs;
  var language = "".obs;
  var userId = 0.obs;

  var rewardDays = 1;

  var user = Rx<User>(null);

  Color get currentDhikrColor {
    if (dhikrColor.value.isEmpty) {
      return Colors.orange[500];
    } else {
      final valueString =
          dhikrColor.value.split('(0x')[1].split(')')[0]; // kind of hacky..
      final value = int.parse(valueString, radix: 16);
      return Color(value);
    }
  }

  void setCurrentDhikrColor(Color color) async {
    dhikrColor.value = color.toString();
    await DataEngine.repositoryOf<AppSettings>()
        .updateColumns(this, ['dhikrColor']);
  }

  void toggleVibrationMode() async {
    vibrationOff.value = vibrationOff.value == 0 ? 1 : 0;
    await DataEngine.repositoryOf<AppSettings>()
        .updateColumns(this, ['vibrationOff']);
  }

  @override
  String toString() => purchased.value == 1 ? "Satın Alındı" : "Demo Version";

  @override
  TableInfo<AppSettings> get tableInfo => TableInfo<AppSettings>(
        tableName: AppSettings.TABLE_NAME,
        resource: "app-settings",
        translation: "entity.app_settings",
        fieldInfos: [
          FieldInfo(
            translation: "entity.app_settings.purchased",
            dataType: DataType.int,
            prop: Prop(
              name: "purchased",
              getter: (e) => e.purchased.value,
              setter: (e, val) => e.purchased.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.app_settings.rewardDate",
            prop: Prop(
              name: "rewardDate",
              getter: (e) => e.rewardDate.value,
              setter: (e, val) => e.rewardDate.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.app_settings.vibrationOff",
            dataType: DataType.int,
            prop: Prop(
              name: "vibrationOff",
              getter: (e) => e.vibrationOff.value,
              setter: (e, val) => e.vibrationOff.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.app_settings.dhikrColor",
            prop: Prop(
              name: "dhikrColor",
              getter: (e) => e.dhikrColor.value,
              setter: (e, val) => e.dhikrColor.value = val,
            ),
          ),
          FieldInfo(
            translation: "entity.app_settings.language",
            newVersion: true,
            prop: Prop(
              name: "language",
              getter: (e) => e.language.value,
              setter: (e, val) => e.language.value = val,
            ),
          ),
        ],
      );

  @override
  int get hashCode =>
      purchased.hashCode ^
      rewardDate.hashCode ^
      vibrationOff.hashCode ^
      dhikrColor.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is AppSettings &&
          purchased == other.purchased &&
          rewardDate == other.rewardDate &&
          vibrationOff == other.vibrationOff &&
          dhikrColor == other.dhikrColor;
}
