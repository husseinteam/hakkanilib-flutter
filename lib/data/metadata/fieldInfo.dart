import 'package:hakkanilib/data/metadata/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FieldInfo<TEntity extends GenericEntity<TEntity>> {
  String get repr => translation.tr;
  final String translation;
  final Prop<TEntity, dynamic> prop;
  final bool newVersion;
  final DataType dataType;
  final bool nullableOnForm;
  final bool displayOnForm;
  final bool primaryKey;
  final bool uniqueKey;
  final dynamic resetTo;
  final bool selfReference;
  final bool local;

  FieldInfo({
    @required this.translation,
    @required this.prop,
    this.newVersion: false,
    this.nullableOnForm: false,
    this.dataType: DataType.text,
    this.displayOnForm: true,
    this.primaryKey: false,
    this.uniqueKey: false,
    this.selfReference: false,
    this.local: false,
    this.resetTo,
  });

  @override
  String toString() {
    final unique = uniqueKey ? " UNIQUE" : "";
    final type = DataTypeRepr.of(dataType);
    return primaryKey
        ? "${prop.name} $type PRIMARY KEY ${selfReference ? 'NULL' : 'NOT NULL'}"
        : "${prop.name} $type$unique";
  }
}
