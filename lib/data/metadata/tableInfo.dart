import 'package:flutter/material.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/metadata/index.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:get/get.dart';

class TableInfo<TEntity extends GenericEntity<TEntity>> {
  String get repr => translation.tr;
  bool get altered => fieldInfos.any((col) => col.newVersion);
  final String translation;
  final String resource;
  final String tableName;
  final Iterable<FieldInfo<TEntity>> fieldInfos;
  final Iterable<ReferenceInfo> referenceInfos;
  final Iterable<SingleReferenceInfo<TEntity>> singleReferences;
  final Iterable<CollectionInfo<TEntity>> collectionInfos;

  TableInfo({
    @required this.translation,
    @required this.resource,
    @required this.tableName,
    @required this.fieldInfos,
    this.referenceInfos,
    this.singleReferences,
    this.collectionInfos,
  }) : assert(fieldInfos.length > 0);

  List<FieldInfo<TEntity>> get fields => List.of([
        FieldInfo<TEntity>(
          translation: 'entity.generic.id',
          prop: Prop(
            name: 'id',
            getter: (e) => e.id.value,
            setter: (e, val) => e.id.value = val,
          ),
          dataType: DataType.int,
          displayOnForm: false,
          primaryKey: true,
        ),
        FieldInfo<TEntity>(
          translation: 'entity.generic.inserted_at',
          prop: Prop(
            name: 'inserted_at',
            getter: (e) => e.insertedAt.string,
            setter: (e, val) => e.insertedAt.value = val,
          ),
          displayOnForm: false,
        ),
        FieldInfo<TEntity>(
          translation: 'entity.generic.updated_at',
          prop: Prop(
            name: 'updated_at',
            getter: (e) => e.updatedAt.string,
            setter: (e, val) => e.updatedAt.value = val,
          ),
          displayOnForm: false,
        ),
      ])
        ..addAll(fieldInfos);

  List<String> get nonBlobCoumns {
    return fields
        .where((fi) => fi.dataType != DataType.blob)
        .map((fi) => fi.prop.name)
        .toList()
          ..addAll(
              (singleReferences ?? []).map((sr) => sr.idProp.name).toList())
          ..addAll((referenceInfos ?? []).map((ri) => ri.idProp.name).toList());
  }

  @override
  String toString() {
    var cols =
        fields.map((col) => "$col").reduce((col1, col2) => "$col1, $col2");
    final filteredReferenceInfos = referenceInfos?.where((ri) =>
        !fieldInfos.map((fi) => fi.prop.name).contains(ri.referencingColumn));
    final refCols =
        (filteredReferenceInfos != null && filteredReferenceInfos.length > 0
                ? (filteredReferenceInfos
                        ?.map((ref) => ", ${ref.columnify()}")
                        ?.reduce((col1, col2) => col1 + col2) ??
                    '')
                : '')
            .trim()
            .stripTrailing(',');
    final refs = (referenceInfos
                ?.map((ref) => ', $ref')
                ?.reduce((ref1, ref2) => ref1 + ref2) ??
            '')
        .trim()
        .stripTrailing(',');
    final singleRefCols = (singleReferences
                ?.map((ref) => ", ${ref.columnify()}")
                ?.reduce((col1, col2) => col1 + col2) ??
            '')
        .trim()
        .stripTrailing(',');
    final singleRefs = singleReferences
        .whereElse(
          (ref) => true,
          exists: (list) =>
              list.map((ref) => ', $ref').reduce((ref1, ref2) => ref1 + ref2),
          noElement: "",
        )
        .trim()
        .stripTrailing(',');
    final parameters = "$cols$refCols$singleRefCols$refs$singleRefs";
    return "CREATE TABLE IF NOT EXISTS $tableName($parameters);";
  }
}
