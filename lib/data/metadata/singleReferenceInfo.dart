import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/repo/index.dart';
import 'package:hakkanilib/data/metadata/index.dart';
import 'package:flutter/material.dart';

class SingleReferenceInfo<TEntity extends GenericEntity<TEntity>> {
  final String foreignTable;
  final Repository referenceRepository;
  final Prop<TEntity, GenericEntity> prop;
  final bool volatile;
  final int Function(dynamic) getForeignId;
  final DataType dataType;
  final String foreignColumn;
  final Prop<TEntity, int> idProp;

  SingleReferenceInfo({
    @required this.foreignTable,
    @required this.referenceRepository,
    @required this.prop,
    @required this.idProp,
    this.volatile: false,
    this.getForeignId,
    this.dataType: DataType.int,
    this.foreignColumn: "id",
  });

  @override
  String toString() {
    return 'FOREIGN KEY(${this.idProp.name}) REFERENCES $foreignTable($foreignColumn)';
  }

  String columnify() {
    final type = DataTypeRepr.of(dataType);
    return "${this.idProp.name} $type NULL";
  }

  String alterStatement() {
    final t = DataEngine.instanceOf(TEntity);
    return "ALTER TABLE ${t.tableInfo.tableName} ADD ${columnify()}";
  }
}
