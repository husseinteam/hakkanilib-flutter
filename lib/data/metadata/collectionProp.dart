import 'package:hakkanilib/data/repo/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:flutter/material.dart';

class CollectionProp<TEntity extends GenericEntity<TEntity>> {
  final Iterable Function(TEntity) getter;
  final TEntity Function(TEntity, Iterable) setter;
  final Repository itemRepo;
  final bool loop;

  CollectionProp({
    @required this.getter,
    @required this.setter,
    this.loop: false,
    this.itemRepo,
  });
}
