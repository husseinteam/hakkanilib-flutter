import 'package:hakkanilib/data/metadata/index.dart';
import 'package:flutter/material.dart';
import 'package:hakkanilib/data/repo/repository.dart';

class ReferenceInfo {
  final String referencingColumn;
  final DataType dataType;
  final String foreignColumn;
  final int Function(dynamic) getForeignId;
  final Repository referenceRepo;
  final Prop prop;
  final Prop idProp;

  ReferenceInfo({
    @required this.referencingColumn,
    @required this.referenceRepo,
    @required this.idProp,
    this.getForeignId,
    this.dataType: DataType.int,
    this.foreignColumn: "id",
    this.prop,
  });

  @override
  String toString() {
    final foreignTable =
        this.referenceRepo.entityInstance().tableInfo.tableName;
    return 'FOREIGN KEY($referencingColumn) REFERENCES $foreignTable($foreignColumn)';
  }

  String columnify() {
    final type = DataTypeRepr.of(dataType);
    return "$referencingColumn $type NOT NULL";
  }
}
