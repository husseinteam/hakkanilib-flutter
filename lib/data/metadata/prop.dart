import 'package:hakkanilib/data/entities/index.dart';
import 'package:flutter/material.dart';

class Prop<TEntity extends GenericEntity<TEntity>, TProp> {
  final String name;
  final TProp Function(TEntity) getter;
  final void Function(TEntity, TProp) setter;
  final TProp Function() defaultValueGetter;
  Prop({
    @required this.name,
    @required this.getter,
    @required this.setter,
    this.defaultValueGetter,
  });
}
