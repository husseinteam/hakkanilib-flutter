import 'package:hakkanilib/data/metadata/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:flutter/material.dart';

class CollectionInfo<TEntity extends GenericEntity<TEntity>> {
  final String name;
  final String referenceName;
  final CollectionProp<TEntity> collectionProp;
  CollectionInfo({
    @required this.name,
    @required this.referenceName,
    @required this.collectionProp,
  });
}
