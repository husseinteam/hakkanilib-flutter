import 'dart:async';
import 'dart:io';

import 'package:get/get.dart';
import 'package:hakkanilib/utils/index.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UIUtils {
  static double modifySizeOf(
      BuildContext context, double Function(Size size) byModifier) {
    return byModifier(MediaQuery.of(context).size);
  }

  static Future<T> actionSheet<T>(
      {@required String title,
      String message,
      Iterable<SheetAction> actions,
      bool dismissible: false}) async {
    final cancelButton = actions.firstWhere(
      (action) => action.isCancelButton,
      orElse: () => null,
    );
    return await Get.bottomSheet(
      CupertinoActionSheet(
        title: Text(
          title,
          style: TextStyle(color: Colors.black),
        ),
        message: message == null || message.isEmpty
            ? null
            : Text(
                message,
                style: TextStyle(color: Colors.black),
              ),
        actions: actions.where((action) => !action.isCancelButton).map(
          (action) {
            List<Widget> actionWidgets = [
              Text(
                action.title,
                style: TextStyle(
                  fontSize: 20.0,
                  color: action.color ?? Colors.black,
                ),
              ),
            ];
            return CupertinoActionSheetAction(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: action.subtitle == null
                    ? actionWidgets
                    : (actionWidgets
                      ..addAll([
                        SizedBox(height: 8.0),
                        Text(
                          action.subtitle,
                          style: TextStyle(
                            fontSize: 12.0,
                            color: action.color ?? Colors.black,
                          ),
                        ),
                      ])),
              ),
              onPressed: () {
                if (action.response != null) {
                  Get.back(result: action.response);
                } else {
                  Get.back();
                }
                if (action.onPress != null) {
                  action.onPress();
                }
              },
              isDefaultAction: action.isDefault,
            );
          },
        ).toList(),
        cancelButton: CupertinoActionSheetAction(
          child: Text(
            cancelButton != null ? cancelButton.title : "alert.cancel".tr,
            style: TextStyle(
              color:
                  cancelButton != null ? cancelButton.color : Colors.redAccent,
            ),
          ),
          isDefaultAction: cancelButton != null ? cancelButton.isDefault : true,
          onPressed: () {
            if (cancelButton != null) {
              if (cancelButton.onPress != null) {
                cancelButton.onPress();
              }
              Get.back(result: cancelButton.response);
            } else {
              Get.back(result: 'cancel');
            }
          },
        ),
      ),
      isDismissible: dismissible,
    );
  }

  static Future<String> alert({
    @required String title,
    @required String message,
    bool multipleChoice: true,
    String approvalText,
    String cancellationText,
    void Function() onApproval,
    void Function() onCancellation,
  }) async {
    return await Get.dialog(
      AlertDialog(
        title: Text(title),
        content: Text(message),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        elevation: 3.0,
        actions: [
          (multipleChoice
              ? TextButton(
                  onPressed: () {
                    if (onCancellation != null) {
                      onCancellation();
                    }
                    Get.back(result: 'cancel');
                  },
                  child: Text(
                    cancellationText ?? "alert.cancel".tr,
                    style: TextStyle(color: Colors.redAccent),
                  ),
                )
              : null),
          TextButton(
            onPressed: () {
              if (onApproval != null) {
                onApproval();
              }
              Get.back(result: 'ok');
            },
            child: Text(
              approvalText ?? "alert.ok".tr,
              style: TextStyle(color: Colors.green),
            ),
          ),
        ],
      ),
      barrierDismissible: false,
    );
  }

  static void snack(
      {String title,
      String message,
      SnackPosition snackPosition: SnackPosition.BOTTOM,
      bool instantInit: true}) async {
    Get.snackbar(
      title,
      message,
      snackPosition: snackPosition,
      instantInit: instantInit,
      colorText: Colors.white,
      duration: Duration(
        milliseconds:
            message.length < 24 ? 2000 : (message.length ~/ 24) * 1200,
      ),
    );
  }

  static Future<bool> checkNetworkStatus() async {
    try {
      final list = await InternetAddress.lookup('example.com').timeout(
        Duration(seconds: 15),
      );
      if (list.isNotEmpty && list[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      return false;
    }
  }
}
