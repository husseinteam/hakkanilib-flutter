import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum AppLocale { tr, en }

const AppLocales = {
  AppLocale.tr: Locale('tr', 'TR'),
  AppLocale.en: Locale('en', 'US'),
};

extension LocaleStringExtensions on String {
  AppLocale get appLocale {
    final lang = this.split(this.indexOf('_') > -1 ? '_' : '-').first;
    switch (lang) {
      case "tr":
        return AppLocale.tr;
      case "en":
        return AppLocale.en;
    }
    return AppLocale.en;
  }
}

extension AppLocaleExtensions on AppLocale {
  String get lang {
    switch (this) {
      case AppLocale.tr:
        return "lang.turkish";
      case AppLocale.en:
        return "lang.english";
    }
    throw UnimplementedError();
  }

  Locale get locale => AppLocales[this];
}

AppLocale get currentAppLocale => AppLocales.keys.firstWhere(
      (apploc) => AppLocales[apploc].languageCode == Get.locale.languageCode,
      orElse: () => null,
    );
