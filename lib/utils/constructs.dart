import 'dart:io';

class Tuple<T1, T2> {
  final T1 it1;
  final T2 it2;
  bool second = false;
  Tuple({
    this.it1,
    this.it2,
  });
  @override
  String toString() {
    return "$it1:$it2";
  }
}

class TupleMono<T> {
  T it1;
  T it2;
  TupleMono<T> previous;
  TupleMono<T> next;
  TupleMono({
    this.it1,
    this.it2,
    this.previous,
    this.next,
  });
  @override
  String toString() {
    return "$it1:$it2";
  }

  TupleMono<T> proceed({bool forwards: true, bool second: false}) {
    if (!second) {
      if (forwards) {
        return this;
      } else {
        return previous;
      }
    } else {
      if (forwards) {
        return next;
      } else {
        return this;
      }
    }
  }
}

class LinkedTuple {
  final int itemCount;
  final coreTuple = TupleMono(it1: 1, it2: 2);

  LinkedTuple(this.itemCount) {
    var next = coreTuple;
    TupleMono<int> prev;
    for (int i in Iterable<int>.generate(itemCount)) {
      var p = i + 1;
      if (p < itemCount) {
        next.next = TupleMono(it1: 2 * (p + 1) - 1, it2: 2 * (p + 1));
        if (p > 0 && prev != null) {
          next.previous = prev;
        }
        prev = next;
        next = next.next;
        next.previous = prev;
      } else {
        next.next = null;
      }
    }
  }

  TupleMono<int> advance({int from, bool forwards: true}) {
    final t = getByOrder(from);
    return forwards ? t.next : t.previous;
  }

  Iterable<TupleMono<int>> iteratePages() sync* {
    TupleMono<int> next = coreTuple;
    do {
      yield next;
      next = next.next;
    } while (next != null);
  }

  TupleMono<int> getByOrder(int order) {
    return iteratePages().firstWhere((t) => t.it1 == order || t.it2 == order,
        orElse: () => iteratePages().last);
  }
}

class KutubiHakkaniHttpGlobalOverride extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
        ..maxConnectionsPerHost = 5;
  }
}

