import 'package:flutter/material.dart';
import 'package:get/get_instance/get_instance.dart';
import 'package:hakkanilib/services/index.dart';

class AsyncService<S> {
  AsyncInstanceBuilderCallback<S> builder;
  String tag;
  bool permanent = false;
}

class StartupConfig {
  final List<AsyncService> asyncServices;
  final Set<String> iapSKUs;

  StartupConfig({
    this.asyncServices: const <AsyncService>[],
    this.iapSKUs: const <String>{},
  });

  void addWidgetsStateObserver(WidgetsStateObserver observer) {
    WidgetsBinding.instance.addObserver(observer);
  }
}
