import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/repo/index.dart';
import 'package:hakkanilib/utils/index.dart';

class UserService extends GetxService {
  Future<UserService> init() async {
    var uRepo = DataEngine.repositoryOf<User>();
    final userCount = await uRepo.count();
    User u;
    if (userCount == 0) {
      u = User()
        ..email.value = "standart"
        ..apiToken.value = ".."
        ..identity.value = "standart"
        ..initials.value = "ss"
        ..password.value = "**"
        ..appSettings.value.language.value = Get.deviceLocale.toString();
      await uRepo.insert(u);
    } else {
      u = (await uRepo.all()).first;
    }
    Get.locale = AppLocales[u.appSettings.value.language.value.appLocale];
    Get.put(u, permanent: true);
    return this;
  }
}
