import 'dart:io';

class AdManager {
  static String get appId {
    if (Platform.isAndroid) {
      return "ca-app-pub-7886619676351440~8115233556";
    } else if (Platform.isIOS) {
      return "ca-app-pub-7886619676351440~8107494698";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      return "";
    } else if (Platform.isIOS) {
      return "";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get interstitialAdUnitId {
    if (Platform.isAndroid) {
      return "";
    } else if (Platform.isIOS) {
      return "";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }

  static String get rewardedAdUnitId {
    if (Platform.isAndroid) {
      return "ca-app-pub-7886619676351440/8857986500";
    } else if (Platform.isIOS) {
      return "ca-app-pub-7886619676351440/1817418516";
    } else {
      throw new UnsupportedError("Unsupported platform");
    }
  }
}
