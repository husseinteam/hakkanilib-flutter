import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/views/controllers/index.dart';
import 'package:hakkanilib/views/repositories/index.dart';

class StartupEngine {
  static final _startupConfig = StartupConfig();
  static RxBool initializeBooks = false.obs;
  static void setResume() {
    _startupConfig.addWidgetsStateObserver(
      WidgetsStateObserver(
        detachedCallBack: () async {
          initializeBooks.value = false;
        },
        resumeCallBack: () async {
          final box = GetStorage();
          final fifteenMinutesBefore =
              DateTime.now().subtract(Duration(minutes: 15));
          final initializedAt =
              box.read<String>("initializedAt")?.toDateTime() ??
                  DateTime.now().subtract(Duration(minutes: 20));
          if (initializedAt.isBefore(fifteenMinutesBefore)) {
            await box.write('initializedAt', DateTime.now().toString());
            initializeBooks.value = true;
          }
        },
      ),
    );
  }

  static Future<void> startup() async {
    WidgetsFlutterBinding.ensureInitialized();
    HttpOverrides.global = KutubiHakkaniHttpGlobalOverride();
    await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    await GetStorage.init();
    setResume();
    // Get.size.shortestSide < 600 ? mobilePhoneOrientations
    await _initServices();
    await _initControllers();
  }

  static Future<void> _initServices() async {
    await DataEngine.init();
    await Get.putAsync(() => UserService().init());
    _startupConfig.asyncServices.forEach(
      (asrv) async => await Get.putAsync(
        asrv.builder,
        tag: asrv.tag,
        permanent: asrv.permanent,
      ),
    );
  }

  static Future<void> _initControllers() async {
    await Get.putAsync(
        () => BooksController(BooksRepository()).init(assignBooks: false));
    await Get.putAsync(
        () => AuthorsController(AuthorsRepository()).init(initBooks: false));
    await Get.putAsync(() =>
        CategoriesController(CategoriesRepository()).init(initBooks: false));
    Get.put(BookDetailController(repository: BooksRepository()));
    final box = GetStorage();
    await box.write('initializedAt', DateTime.now().toString());
    initializeBooks.value = true;
  }
}
