import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hakkanilib/views/home/index.dart';

class AppPage<TController extends GetxController> extends StatelessWidget {
  TController get controller => Get.find<TController>();

  Widget get title => Obx(() => Text(
        titleIsKey.value ? titleKey.value.tr : titleKey.value,
        style: TextStyle(fontSize: 18.0),
      ));

  final RxList<Widget> actions = <Widget>[].obs;
  final Rx<Widget> icon = Rx<Icon>(null);
  final RxString titleKey = "".obs;
  final RxString alternativeTranslationKey = RxString(null);

  Widget get floatingButton => null;
  final RxBool titleIsKey = true.obs;

  Function() get onTap => () => {};

  AppBar get appBar => _buildAppBar(title, actions, buildDrawer: true);

  Widget buildThe(Widget center, {bool withDrawer: false}) {
    return Scaffold(
      appBar: withDrawer ? appBar : _buildAppBar(title, actions),
      floatingActionButton: floatingButton != null
          ? Obx(() => Padding(
                padding: EdgeInsets.only(
                  bottom: HomeEngine.fabElevation.value,
                ),
                child: floatingButton,
              ))
          : null,
      body: SafeArea(child: center),
    );
  }

  AppBar _buildAppBar(
    Widget title,
    List<Widget> actions, {
    bool buildDrawer: false,
  }) =>
      buildDrawer == false
          ? AppBar(
              title: title,
              actions: actions,
              titleSpacing: 0.0,
            )
          : AppBar(
              title: title,
              actions: actions,
              // leading: Builder(
              //   builder: (BuildContext context) {
              //     return IconButton(
              //       icon: Icon(Icons.menu, color: Constants.buttonColor),
              //       onPressed: () {
              //         Scaffold.of(context).openDrawer();
              //       },
              //     );
              //   },
              // ),
            );

  @override
  Widget build(BuildContext context) {
    throw UnimplementedError();
  }
}
