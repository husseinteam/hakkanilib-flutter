import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/controllers/index.dart';
import 'package:hakkanilib/views/repositories/index.dart';

class AuthorsController extends GetxController {
  final AuthorsRepository authorRepo;
  AuthorsController(this.authorRepo) : assert(authorRepo != null);

  Future<AuthorsController> init({bool initBooks: true}) async {
    authors.assignAll(await this.authorRepo.getListOf<Author>());
    if (initBooks) {
      await Get.find<BooksController>().init();
    }
    StartupEngine.initializeBooks.value = false;
    return this;
  }

  Future<List<Author>> searchAuthors(String criteria) async {
    criteria = criteria.toLowerCase();
    return authors
        .where((b) =>
            b.fullName.toLowerCase().startsWith(criteria) ||
            b.fullName.toLowerCase().indexOf(criteria) > -1)
        .toList();
  }

  AuthorsController setBooksOfAuthor(Author author) {
    final books = Get.find<BooksController>().books;
    this.books.assignAll(
        books.where((book) => book.authorId.value == author.id.value).toList());
    return this;
  }

  final authors = <Author>[].obs;

  final books = <Book>[].obs;
}
