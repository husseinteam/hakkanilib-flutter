import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/repositories/index.dart';
import 'package:meta/meta.dart';
import 'package:native_pdf_view/native_pdf_view.dart';

class BookDetailController extends GetxController {
  final BooksRepository repository;

  var documentLoaded = false.obs;
  int pageCount;
  PdfController pdfCtrl;

  BookDetailController({@required this.repository})
      : assert(repository != null);

  void init() {
    documentLoaded.value = false;
    pdfCtrl = null;
  }

  Book book;

  Future<void> setPage(int page) async {
    book.lastPageOrder.value = page;
    await DataEngine.repositoryOf<Book>()
        .updateColumns(book, ['lastPageOrder']);
  }

  void begin(count) {
    documentLoaded.value = true;
    pageCount = count;
  }

  void next() async {
    if (pageCount == null && pdfCtrl == null) {
      return;
    }
    final curPage = book.lastPageOrder.value;
    if (curPage < pageCount) {
      await pdfCtrl.animateToPage(
        curPage + 1,
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    }
  }

  void prev() async {
    if (pageCount == null && pdfCtrl == null) {
      return;
    }
    final curPage = book.lastPageOrder.value;
    if (curPage > 0) {
      await pdfCtrl.animateToPage(
        curPage - 1,
        duration: Duration(milliseconds: 300),
        curve: Curves.easeInOut,
      );
    }
  }
}
