import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/repositories/index.dart';
import 'package:path_provider/path_provider.dart';

class BooksController extends GetxController {
  final BooksRepository booksRepo;

  BooksController(this.booksRepo) : assert(booksRepo != null);

  Future<BooksController> init({bool assignBooks: true}) async {
    this._appPath = (await getApplicationDocumentsDirectory()).path;
    if (assignBooks) {
      books.assignAll(
          await this.booksRepo.getListOf<Book>({'appPath': _appPath}));
    }
    StartupEngine.initializeBooks.value = false;
    return this;
  }

  Future<List<Book>> searchBooks(String criteria) async {
    criteria = criteria.toLowerCase();
    return books
        .where((b) =>
            b.title.toLowerCase().startsWith(criteria) ||
            b.title.toLowerCase().indexOf(criteria) > -1)
        .toList();
  }

  final books = <Book>[].obs;
  String _appPath;
  String get appPath => _appPath;
}
