import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/controllers/index.dart';
import 'package:hakkanilib/views/repositories/index.dart';

class CategoriesController extends GetxController {
  final CategoriesRepository categoryRepo;

  CategoriesController(this.categoryRepo) : assert(categoryRepo != null);

  Future<CategoriesController> init({bool initBooks: true}) async {
    categories.assignAll(await this.categoryRepo.getListOf<Category>());
    if (initBooks) {
      await Get.find<BooksController>().init();
    }
    StartupEngine.initializeBooks.value = false;
    return this;
  }

  Future<List<Category>> searchCategories(String criteria) async {
    criteria = criteria.toLowerCase();
    return categories
        .where((b) =>
            b.title.toLowerCase().startsWith(criteria) ||
            b.title.toLowerCase().indexOf(criteria) > -1)
        .toList();
  }

  CategoriesController setBooksOfCategory(Category category) {
    final books = Get.find<BooksController>().books;
    this.books.assignAll(books
        .where((book) => book.categoryId.value == category.id.value)
        .toList());
    return this;
  }

  final categories = <Category>[].obs;

  final books = <Book>[].obs;
}
