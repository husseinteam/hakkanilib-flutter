import 'dart:typed_data';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/home/index.dart';
import 'package:hakkanilib/views/pages/index.dart';
import 'package:hakkanilib/utils/index.dart';

class BooksRepository {
  BooksRepository();

  var refetchCount = 0.obs;
  var willDownloadFromInternet = false.obs;
  var cannotDownloadFromInternet = false.obs;

  Future<List<T>> getListOf<T extends GenericEntity<T>>(
      [Map<String, dynamic> args]) async {
    final cached = () async {
      final list = await DataEngine.repositoryOf<T>().all();
      for (var item in list) {
        await item.fixReferences();
      }
      return list;
    };
    if (await UIUtils.checkNetworkStatus()) {
      return await _networkIsOnline(args);
    } else {
      return await cached();
    }
  }

  Future<List<T>> _networkIsOnline<T extends GenericEntity<T>>(
      [Map<String, dynamic> args]) async {
    final fetched = await DataEngine.fetcherOf<T>().fetchMany();
    final repo = DataEngine.repositoryOf<T>();
    final fixNextBookReference = (T e) async {
      if (e is Book) {
        final b = e as Book;
        b.nextBookId.value = null;
        await repo.updateColumns(b as T, ['next_id']);
      }
    };

    final allLocal = await repo.all(digin: false);
    if (fetched.length == 0) {
      for (var local in allLocal) {
        await fixNextBookReference(local);
      }
      await repo.deleteAll();
    }

    // allLocal.length > fetched.length
    // extra element exists in allLocal so it must be deleted
    for (var local in allLocal) {
      // if fetched list excludes local element that should be the extra element
      if (!fetched.any((remote) => local.id.value == remote.id.value)) {
        await fixNextBookReference(local);
        await repo.delete(local);
      }
    }

    final list = <T>[];
    for (var remote in fetched) {
      dynamic local;
      Poster remotePoster;
      if (remote is Book) {
        remotePoster = await DataEngine.repositoryOf<Poster>().upsert(
          (remote as Book).poster,
        );
      }
      local = await repo.upsert(remote);
      if (local is Book) {
        local.poster = remotePoster;
        await setPosterName(remotePoster, appPath: args['appPath']);
      }
      list.add(local);
    }

    return list;
  }

  Widget buildBookGrid(List<Book> books, String noBookMessageKey) {
    final tabs = <int, Widget>{
      0: Text("title.show_all_books".tr),
      1: Text("title.show_favorites".tr)
    };
    final booksWidget = books.length == 0
        ? Center(child: Text(noBookMessageKey.tr))
        : Obx(() => GridView.count(
              crossAxisCount: Get.context.isPhone ? 2 : 3,
              childAspectRatio: 0.9,
              children: books.map((book) => _buildBookTile(book)).toList(),
            ));
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: CupertinoSlidingSegmentedControl(
              groupValue: HomeEngine.showingFavorites.value ? 1 : 0,
              children: tabs,
              thumbColor: Constants.lightColor,
              backgroundColor: Constants.notrColor,
              padding: EdgeInsets.all(4.0),
              onValueChanged: (i) {
                HomeEngine.showingFavorites.value = i == 1;
              }),
        ),
        Expanded(
          child: booksWidget,
        ),
      ],
    );
  }

  Widget _buildBookTile(Book book) {
    refetchCount.value = 0;
    return Container(
      margin: EdgeInsets.all(4.0),
      child: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Column(
          children: [
            Expanded(
              flex: Get.context.isPortrait ? 4 : 2,
              child: GestureDetector(
                onTap: () async {
                  var response = "ok";
                  final bookDataFieldLength =
                      await DataEngine.repositoryOf<Book>().blobLength(
                          book, book.fieldInfo(withName: "bookData"));
                  if (bookDataFieldLength == 0) {
                    if (await UIUtils.checkNetworkStatus()) {
                      response = await UIUtils.alert(
                        title: "alert.continue_on_process".tr,
                        message: "message.pdf_content_info".trArgs(
                          [book.pdfSize.value.bytesAsMB(), book.title.string],
                        ),
                      );
                    } else {
                      await UIUtils.alert(
                        title: "title.failed_operation".tr,
                        message: "error.no_internet".tr,
                        multipleChoice: false,
                      );
                    }
                  }
                  if (response == "ok") {
                    Get.to(() => BookDetailPage(book));
                  }
                },
                child: _buildImage(book),
              ),
            ),
            _buildGridTileBar(book),
          ],
        ),
      ),
    );
  }

  Container _buildImage(Book book) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(5),
        child: Image.file(
          book.posterFile,
          fit: BoxFit.fitHeight,
          errorBuilder: (_, o, st) {
            return FutureBuilder<String>(
              future: setPosterName(book.poster, forceFetch: true),
              builder: (_, ss) {
                if (ss.data == null) {
                  return Center(child: CircularProgressIndicator());
                } else {
                  return Image.file(book.posterFile, fit: BoxFit.fitHeight);
                }
              },
            );
          },
        ),
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.white, width: 1.0),
        borderRadius: BorderRadius.all(Radius.circular(5)),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.29),
            spreadRadius: 3,
            blurRadius: 7,
          ),
        ],
      ),
    );
  }

  Widget _buildGridTileBar(Book book) {
    final evalFontSize = (double n) =>
        Get.context.mediaQueryShortestSide *
        (n / (Get.context.isPhone ? 375 : 768));
    return Expanded(
      child: GestureDetector(
        onTap: () async => await this.toggleIsFavorite(book),
        child: GridTileBar(
          title: Obx(() => Text(
                book.title.string,
                overflow: TextOverflow.fade,
                softWrap: true,
                style: TextStyle(
                  fontSize: evalFontSize(Get.context.isPhone ? 10 : 15),
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              )),
          subtitle: Obx(() => Text(
                book.author.value.fullName.string,
                overflow: TextOverflow.fade,
                softWrap: true,
                style: TextStyle(
                  fontSize: evalFontSize(Get.context.isPhone ? 8 : 13),
                  color: Colors.black,
                ),
              )),
          backgroundColor: Colors.white.withOpacity(0.0),
          leading: Obx(() => Icon(
                book.isFavorite.value == 1 ? Icons.star : Icons.star_border,
                size: 18.0,
                color: Colors.black87,
              )),
        ),
      ),
    );
  }

  Future<String> setPosterName(
    Poster poster, {
    bool forceFetch: false,
    String appPath,
  }) async {
    final posterName = poster.posterName.value;
    final file = Book.localFile(withName: posterName, appPath: appPath);
    if (await UIUtils.checkNetworkStatus()) {
      if (forceFetch == false &&
          poster.differs(
            withFields: ['updated_at'],
            fixDifferences: true,
          )) {
        return await setPosterName(poster, forceFetch: true);
      } else if (forceFetch == true || !file.existsSync()) {
        final data =
            await DataEngine.fetcherOf<Poster>().fetchHttpPost<Uint8List>(
          'download/poster',
          {'posterName': posterName, 'isThumbnail': true},
          responseByteArray: true,
        );
        await file.writeAsBytes(data, flush: true);
      }
    }
    return posterName;
  }

  Future<Uint8List> getBookData(Book book, {bool forceFetch: false}) async {
    await DataEngine.repositoryOf<Book>().processBlob(
      of: book,
      processFields: [book.fieldInfo(withName: "bookData")],
    );
    willDownloadFromInternet.value = forceFetch;
    if (await UIUtils.checkNetworkStatus()) {
      cannotDownloadFromInternet.value = false;
      if (forceFetch == false &&
          book.differs(
            withFields: ['updated_at'],
            fixDifferences: true,
          )) {
        return await getBookData(book, forceFetch: true);
      } else if (forceFetch == true || book.bookData.value == null) {
        willDownloadFromInternet.value = true;
        final data =
            await DataEngine.fetcherOf<Book>().fetchHttpPost<Uint8List>(
          'download/book',
          {'pdfName': book.pdfName.value},
          responseByteArray: true,
        );
        book.bookData.value = data;
        await DataEngine.repositoryOf<Book>().updateColumns(book, ['bookData']);
      }
    } else {
      cannotDownloadFromInternet.value = true;
    }
    return book.bookData.value;
  }

  Future<void> toggleIsFavorite(Book book) async {
    book.isFavorite.value = book.isFavorite.value == 0 ? 1 : 0;
    await DataEngine.repositoryOf<Book>().updateColumns(book, ['isFavorite']);
  }
}
