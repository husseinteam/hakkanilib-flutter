import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hakkanilib/components/forms/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/models/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/controllers/index.dart';
import 'package:hakkanilib/views/index.dart';
import 'package:get/get.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/views/pages/authors_detail_page.dart';

class AuthorsPage extends AppPage<AuthorsController> {
  final selectedAuthor = Rx<Author>(null);

  @override
  get titleKey => 'title.authors_page'.obs;

  @override
  get icon => FaIcon(FontAwesomeIcons.users).obs;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(() => StartupEngine.initializeBooks.value
            ? FutureBuilder<AuthorsController>(
                future: controller.init(),
                builder: (_, ss) {
                  if (ss.data == null) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    return _buildBody(ss.data.authors);
                  }
                },
              )
            : _buildBody(controller.authors)),
      ),
    );
  }

  Widget _buildBody(List<Author> authors) {
    return Obx(() => authors.length == 0
        ? Center(child: Text("message.no_authors".tr))
        : OptionsForm.of<AuthorModel>(
            options: authors.map((a) => AuthorModel(a)).toList(),
            onItemTap: (item) {
              AuthorsDetailPage(item as Author).show();
            },
          ));
  }
}
