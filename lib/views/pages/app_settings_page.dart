import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:hakkanilib/views/home/index.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:hakkanilib/components/forms/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/models/index.dart';
import 'package:hakkanilib/views/index.dart';

class AppSettingsPage extends AppPage<User> {
  @override
  get titleKey => 'title.app_settings_page'.obs;

  @override
  get icon => Icon(Icons.settings).obs;

  @override
  Widget build(BuildContext context) {
    return SettingsList(
      sections: [
        SettingsSection(
          title: 'title.section_system'.tr,
          tiles: [
            SettingsTile(
              title: 'title.language'.tr,
              subtitle: currentAppLocale.lang.tr,
              leading: FaIcon(
                FontAwesomeIcons.language,
                color: Colors.blueAccent,
              ),
              onPressed: (ctx) async {
                final model = await OptionsForm.of<LanguageModel>(
                  titleKey: 'title.language',
                  options: LanguageModel.appLocaleOptions,
                ).show();
                if (model != null) {
                  await controller.updateLocale(AppLocales[model.appLocale]);
                  HomeEngine.tapAt();
                }
              },
            ),
          ],
        ),
        SettingsSection(
          title: 'title.section_among_us'.tr,
          tiles: [
            SettingsTile(
              title: 'title.about_us'.tr,
              subtitle: 'title.what_we_do'.tr,
              leading: Icon(Icons.account_box, color: Constants.darkColor),
              enabled: true,
              onPressed: (ctx) => AboutPage().show(),
            ),
          ],
        ),
      ],
    );
  }
}
