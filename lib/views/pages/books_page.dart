import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/controllers/index.dart';
import 'package:hakkanilib/views/home/index.dart';
import 'package:hakkanilib/views/index.dart';
import 'package:get/get.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BooksPage extends AppPage<BooksController> {
  @override
  get titleKey => 'title.books_page'.obs;

  @override
  get icon => FaIcon(FontAwesomeIcons.bookOpen).obs;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(() {
          final books = HomeEngine.showingFavorites.value
              ? controller.books.where((b) => b.isFavorite.value == 1).toList()
              : controller.books;
          return StartupEngine.initializeBooks.value
              ? FutureBuilder<BooksController>(
                  future: controller.init(),
                  builder: (_, ss) {
                    if (ss.data == null) {
                      return Center(child: CircularProgressIndicator());
                    } else {
                      return controller.booksRepo
                          .buildBookGrid(ss.data.books, "message.no_books_yet");
                    }
                  },
                )
              : controller.booksRepo
                  .buildBookGrid(books, "message.no_books_yet");
        }),
      ),
    );
  }
}
