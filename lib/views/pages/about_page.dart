import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/views/index.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:get/get.dart';

class AboutPage extends AppPage {
  @override
  get titleKey => 'title.about_page'.obs;

  @override
  get icon => Icon(Icons.account_box).obs;

  @override
  Widget build(BuildContext context) {
    return super.buildThe(
      Container(
        child: Center(
          child: Align(
            alignment: Alignment.center,
            child: Container(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 30.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        final launcher = ({bool builtin: true}) async {
                          const url = 'http://hakkani.org';
                          if (await canLaunch(url)) {
                            await launch(url, forceSafariVC: builtin);
                          } else {
                            UIUtils.snack(
                              title: 'title.failed_operation'.tr,
                              message:
                                  'message.redirect_to_hakkanitv_failed'.tr,
                            );
                          }
                        };
                        UIUtils.actionSheet(
                          title: "alert.continue_on_process".tr,
                          message: "message.will_be_redirected_to_hakkanitv".tr,
                          dismissible: true,
                          actions: Platform.isIOS
                              ? [
                                  SheetAction(
                                    title: "message.open_in_builtin_browser".tr,
                                    color: Colors.green,
                                    onPress: launcher,
                                  ),
                                  SheetAction(
                                    title: "message.open_in_device_browser".tr,
                                    color: Colors.blue,
                                    onPress: () async =>
                                        await launcher(builtin: false),
                                  )
                                ]
                              : [
                                  SheetAction(
                                    title: "alert.continue".tr,
                                    color: Colors.green,
                                    onPress: launcher,
                                  )
                                ],
                        );
                      },
                      child: Image.asset("assets/images/widgets/promotion.png"),
                    ),
                    Text(
                      'app_name'.tr.toUpperCase(),
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(height: 11.0),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 21.0),
                      child: Text(
                        'title.about_hakkanilib'.tr,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Constants.darkColor),
                      ),
                    ),
                    SizedBox(height: 11.0),
                    Text(
                      'text.about_hakkanilib'.tr,
                      style:
                          TextStyle(fontSize: 14, color: Constants.darkColor),
                      textAlign: TextAlign.justify,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
