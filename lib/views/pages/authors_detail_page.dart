import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/controllers/index.dart';
import 'package:hakkanilib/views/home/home_engine.dart';
import 'package:hakkanilib/views/index.dart';
import 'package:get/get.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AuthorsDetailPage extends AppPage<AuthorsController> {
  final Author selectedAuthor;

  AuthorsDetailPage(this.selectedAuthor);

  @override
  get titleKey => (selectedAuthor == null
          ? 'title.authors_page'
          : selectedAuthor.fullName.string)
      .obs;

  @override
  get icon => FaIcon(FontAwesomeIcons.users).obs;

  @override
  Widget build(BuildContext context) {
    return buildThe(
      SafeArea(
        child: Obx(() {
          final getBooks = (books) => HomeEngine.showingFavorites.value
              ? books.where((b) => b.isFavorite.value == 1).toList()
              : books;
          return StartupEngine.initializeBooks.value
              ? FutureBuilder<AuthorsController>(
                  future: controller.init(),
                  builder: (_, ss) {
                    if (ss.data == null) {
                      return Center(child: CircularProgressIndicator());
                    } else {
                      return _buildBody(
                        getBooks(
                            ss.data.setBooksOfAuthor(selectedAuthor).books),
                      );
                    }
                  },
                )
              : _buildBody(
                  getBooks(controller.setBooksOfAuthor(selectedAuthor).books));
        }),
      ),
    );
  }

  Widget _buildBody(List<Book> books) {
    return controller.authorRepo
        .buildBookGrid(books, "message.no_book_of_author");
  }
}
