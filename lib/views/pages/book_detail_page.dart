import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hakkanilib/components/forms/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/models/page_model.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/views/controllers/index.dart';
import 'package:hakkanilib/views/index.dart';
import 'package:get/get.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:native_pdf_view/native_pdf_view.dart';
import 'package:toast/toast.dart';

class BookDetailPage extends AppPage<BookDetailController> {
  BookDetailPage(Book book) {
    controller.book = book;
  }

  @override
  RxList<Widget> get actions => [
        Obx(() => controller.documentLoaded.value
            ? IconButton(
                onPressed: controller.prev,
                icon: Icon(
                  Icons.arrow_back_ios,
                  size: 18.0,
                  color: Constants.buttonColor,
                ),
              )
            : Container()),
        Obx(() => controller.documentLoaded.value
            ? Center(
                child: TextButton(
                  child: Obx(() => Text(
                        "${controller.book.lastPageOrder}/${controller.pdfCtrl.pagesCount}",
                        style: TextStyle(color: Constants.labelColor),
                      )),
                  onPressed: () async {
                    final pageModel = await Get.to<PageModel>(
                      () => OptionsForm.of<PageModel>(
                        titleKey: 'title.please_select_a_page',
                        options:
                            PageModel.options(controller.pdfCtrl.pagesCount)
                                .toList(),
                        jumpToOption: controller.book.lastPageOrder.toInt(),
                      ),
                      fullscreenDialog: true,
                    );
                    if (pageModel != null) {
                      Future.delayed(Duration(milliseconds: 100), () {
                        controller.pdfCtrl.jumpToPage(pageModel.page);
                      });
                    }
                  },
                ),
              )
            : Container()),
        Obx(() => controller.documentLoaded.value
            ? IconButton(
                onPressed: controller.next,
                icon: Icon(
                  Icons.arrow_forward_ios,
                  size: 18.0,
                  color: Constants.buttonColor,
                ),
              )
            : Container()),
      ].obs;

  @override
  Widget get title => Text(controller.book.title.string);

  @override
  get icon => FaIcon(FontAwesomeIcons.bookOpen).obs;

  @override
  Widget build(BuildContext context) {
    controller.init();
    return buildThe(
      Center(
        child: _buildBookBuilder(),
      ),
    );
  }

  Widget _buildBookBuilder() {
    return FutureBuilder<Uint8List>(
      key: ValueKey(controller.book.id.value),
      future: controller.repository.getBookData(controller.book),
      builder: (ctx, snapshot) {
        if (snapshot.data == null) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator(),
                SizedBox(height: 14.0),
                Obx(() => Text((controller
                            .repository.cannotDownloadFromInternet.value
                        ? 'message.book_cannot_download'
                        : controller.repository.willDownloadFromInternet.value
                            ? 'message.book_downloading'
                            : 'message.book_loading')
                    .tr)),
              ],
            ),
          );
        } else {
          return _buildPdfView(snapshot.data, ctx);
        }
      },
    );
  }

  PdfView _buildPdfView(Uint8List data, BuildContext ctx) {
    controller.pdfCtrl = PdfController(
      document: PdfDocument.openData(data).catchError((_, st) async {
        final data = await controller.repository
            .getBookData(controller.book, forceFetch: true);
        return PdfDocument.openData(data);
      }),
    );
    return PdfView(
      controller: controller.pdfCtrl,
      documentLoader: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
            SizedBox(height: 14.0),
            Text('message.book_loading'.tr),
          ],
        ),
      ),
      onDocumentLoaded: (doc) {
        controller.begin(doc.pagesCount);
        Toast.show(
          "message.document_loaded".tr,
          ctx,
          duration: Toast.LENGTH_SHORT,
          gravity: Toast.CENTER,
        );
        Future.delayed(
          Duration(milliseconds: 100),
          () {
            final lastPage = controller.book.rtl.value == 0
                ? controller.book.lastPageOrder.value
                : (controller.book.lastPageOrder.value == 1
                    ? doc.pagesCount
                    : controller.book.lastPageOrder.value);
            controller.pdfCtrl.jumpToPage(lastPage);
          },
        );
      },
      onPageChanged: (page) async {
        await controller.setPage(page);
      },
    );
  }
}
