import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hakkanilib/components/forms/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/models/index.dart';
import 'package:hakkanilib/services/index.dart';
import 'package:hakkanilib/views/controllers/index.dart';
import 'package:hakkanilib/views/index.dart';
import 'package:get/get.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/views/pages/index.dart';

class CategoriesPage extends AppPage<CategoriesController> {

  @override
  get titleKey => 'title.categories_page'.obs;

  @override
  get icon => Icon(Icons.category).obs;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Obx(() => StartupEngine.initializeBooks.value
            ? FutureBuilder<CategoriesController>(
                future: controller.init(),
                builder: (_, ss) {
                  if (ss.data == null) {
                    return Center(child: CircularProgressIndicator());
                  } else {
                    return _buildBody(ss.data.categories);
                  }
                },
              )
            : _buildBody(controller.categories)),
      ),
    );
  }

  Widget _buildBody(List<Category> categories) {
    return Obx(() => categories.length == 0
        ? Center(child: Text("message.no_categories".tr))
        : OptionsForm.of<CategoryModel>(
            options: categories.map((a) => CategoryModel(a)).toList(),
            onItemTap: (item) {
              CategoriesDetailPage(item as Category).show();
            },
          ));
  }
}
