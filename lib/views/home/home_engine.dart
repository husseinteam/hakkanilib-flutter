import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/views/index.dart';
import 'package:hakkanilib/views/pages/index.dart';

class HomeEngine {
  static final pages = <AppPage>[
    BooksPage(),
    AuthorsPage(),
    CategoriesPage(),
    AppSettingsPage(),
  ];

  static RxDouble fabElevation = 0.0.obs;
  static RxInt pageIndex = 0.obs;
  static AppBar appBar;
  static Widget floatingButton;
  static final RxBool backPressedOnce = false.obs;
  static final RxBool showingFavorites = false.obs;

  static final _gests = pages
      .asMap()
      .map(
        (index, page) => MapEntry<int, GestureDetector>(
          index,
          GestureDetector(
            onTap: () {
              pageIndex.value = index;
              setParams(index);
              if (page.onTap != null) {
                page.onTap();
              }
            },
          ),
        ),
      )
      .values
      .toList();

  static void tapAt({int index: 0}) {
    _gests.elementAt(index).onTap();
  }

  static void setParams(int pageIndex) {
    final ctrl = pages[pageIndex];
    appBar = AppBar(
      toolbarHeight: 66.0,
      title: ctrl.title,
      actions: ctrl.actions,
      // leading: Builder(
      //   builder: (BuildContext context) {
      //     return IconButton(
      //       icon: Icon(Icons.menu, color: Constants.buttonColor),
      //       onPressed: () {
      //         Scaffold.of(context).openDrawer();
      //       },
      //     );
      //   },
      // ),
    );
    floatingButton = ctrl.floatingButton;
  }

  static Widget get bottomNavigationBar {
    return BottomNavigationBar(
      currentIndex: pageIndex.value,
      type: BottomNavigationBarType.fixed,
      iconSize: 20.0,
      selectedFontSize: 10.0,
      unselectedFontSize: 10.0,
      selectedItemColor: Constants.intenseColor,
      unselectedItemColor: Constants.labelColor,
      backgroundColor: Constants.backgroundColor,
      items: pages
          .map((page) => BottomNavigationBarItem(
                icon: page.icon.value,
                label: (page.alternativeTranslationKey.value
                        .ifNull(elseThen: page.titleKey.value))
                    .tr,
              ))
          .toList(),
      onTap: (index) {
        _gests.elementAt(index).onTap();
      },
    );
  }
}
