import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/views/home/index.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(context) {
    HomeEngine.setParams(0);
    final customFontTextTheme =
        GoogleFonts.openSansTextTheme(Theme.of(context).textTheme).copyWith(
      button: GoogleFonts.openSans(
        color: Constants.buttonColor,
        textStyle: TextStyle(fontSize: 15),
      ),
      caption: GoogleFonts.openSans(
        textStyle: TextStyle(color: Constants.labelColor),
      ),
      bodyText1: GoogleFonts.openSans(
        textStyle: TextStyle(color: Constants.labelColor),
      ),
      bodyText2: GoogleFonts.openSans(
        textStyle: TextStyle(color: Constants.labelColor),
      ),
      headline6: GoogleFonts.openSans(
        textStyle: TextStyle(color: Constants.labelColor),
      ),
    );
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      translations: Messages(),
      locale: Get.locale,
      fallbackLocale: AppLocales[AppLocale.en],
      defaultTransition: Platform.isIOS
          ? Transition.cupertino
          : Transition.leftToRightWithFade,
      opaqueRoute: Get.isOpaqueRouteDefault,
      popGesture: Get.isPopGestureEnable,
      theme: ThemeData(
        fontFamily: 'OpenSans',
        primarySwatch: Constants.darkColor.material,
        colorScheme: Theme.of(context).colorScheme.copyWith(
              primary: Constants.buttonColor,
            ),
        primaryTextTheme: customFontTextTheme,
        appBarTheme: AppBarTheme(
          textTheme: customFontTextTheme,
          iconTheme: IconThemeData(color: Constants.buttonColor),
          actionsIconTheme: IconThemeData(color: Constants.buttonColor),
        ),
      ),
      home: Obx(() => Scaffold(
            appBar: HomeEngine.appBar,
            floatingActionButton: HomeEngine.floatingButton != null
                ? Obx(() => Padding(
                      padding: EdgeInsets.only(
                        bottom: HomeEngine.fabElevation.value,
                      ),
                      child: HomeEngine.floatingButton,
                    ))
                : null,
            // drawer: _buildDrawer(context),
            body: _buildBody(),
            bottomNavigationBar: HomeEngine.bottomNavigationBar,
          )),
    );
  }

  Widget _buildBody() {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: SafeArea(
        child: HomeEngine.pages[HomeEngine.pageIndex.value],
      ),
    );
  }

  // Drawer _buildDrawer(BuildContext context) {
  //   return Drawer(
  //     child: ListView(
  //       children: [
  //         Image.asset(
  //           'assets/images/widgets/hakkanilib-screen.png',
  //           fit: BoxFit.fill,
  //         ),
  //         ListTile(
  //           title: Text(
  //             "title.show_all_books".tr,
  //             style: TextStyle(color: Constants.darkColor),
  //           ),
  //           leading: Icon(
  //             Icons.star_border,
  //             color: Constants.darkColor,
  //           ),
  //           onTap: () async {
  //             HomeEngine.showingFavorites.value = false;
  //             Get.back();
  //           },
  //         ),
  //         ListTile(
  //           title: Text(
  //             "title.show_favorites".tr,
  //             style: TextStyle(color: Constants.darkColor),
  //           ),
  //           leading: Icon(
  //             Icons.star,
  //             color: Constants.darkColor,
  //           ),
  //           onTap: () async {
  //             HomeEngine.showingFavorites.value = true;
  //             Get.back();
  //           },
  //         ),
  //       ],
  //     ),
  //   );
  // }

  Future<bool> _onBackPressed() {
    if (HomeEngine.backPressedOnce.value == false) {
      UIUtils.snack(
        title: "title.about_to_exit".tr,
        message: "message.about_to_exit".tr,
      );
      HomeEngine.backPressedOnce.value = true;
      return Future.value(false);
    } else {
      return Future.value(true);
    }
  }
}
