import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:hakkanilib/utils/constants.dart';
import 'package:hakkanilib/utils/index.dart';

abstract class FormOption {
  Widget get title;
  Widget get subtitle;
  get item;
}

typedef _Form NextFormCallback(FormOption withModel);

class OptionsForm {
  static _Form<T> of<T extends FormOption>({
    String titleKey,
    List<FormOption> options,
    NextFormCallback nextForm,
    Function(dynamic) onItemTap,
    int jumpToOption: 0,
  }) {
    return _Form(titleKey, options, nextForm, onItemTap, jumpToOption);
  }
}

class _Form<T extends FormOption> extends StatelessWidget {
  final String titleKey;
  final List<T> options;
  final NextFormCallback nextForm;
  final Function(dynamic) onItemTap;
  final int jumpToOption;
  int get jumpTo =>
      jumpToOption - (Get.mediaQuery.size.longestSide ~/ itemHeight) ~/ 2;
  final _controller = ScrollController();
  final itemHeight = 64.0;

  _Form(
    this.titleKey,
    this.options,
    this.nextForm,
    this.onItemTap,
    this.jumpToOption,
  );

  @override
  Widget build(BuildContext context) {
    final widget = this.titleKey != null
        ? Scaffold(
            appBar: AppBar(
              title: Text(
                this.titleKey.tr,
              ),
              actions: [
                TextButton(
                  child: Text(
                    "alert.cancel".tr,
                    style: TextStyle(color: Constants.buttonColor),
                  ),
                  onPressed: () {
                    Get.back();
                  },
                )
              ],
            ),
            body: _buildBody(),
          )
        : _buildBody();
    Future.delayed(Duration(milliseconds: 300), () {
      if (jumpTo > 0) {
        _controller.animateTo(
          itemHeight * jumpTo,
          duration: Duration(seconds: 1),
          curve: Curves.fastOutSlowIn,
        );
      }
    });
    return widget;
  }

  Icon iconOf(_Form nform, {bool leading: true}) {
    if (leading) {
      if (onItemTap == null) {
        return nform == null ? Icon(Icons.arrow_back_ios) : null;
      } else {
        return null;
      }
    } else {
      if (onItemTap == null) {
        return nform != null ? Icon(Icons.arrow_forward_ios) : null;
      } else {
        return Icon(Icons.arrow_forward_ios);
      }
    }
  }

  Widget _buildBody() {
    return options.length > 0
        ? ListView(
            controller: _controller,
            padding: EdgeInsets.symmetric(vertical: 10),
            children: options
                .asMap()
                .map((i, option) {
                  final nform = this.nextForm?.call(option);
                  return MapEntry<int, Widget>(
                    i,
                    SizedBox(
                      height: this.itemHeight,
                      child: ListTile(
                        tileColor: i == jumpToOption - 1
                            ? Constants.backgroundColor.withAlpha(129)
                            : null,
                        leading: iconOf(nform),
                        trailing: iconOf(nform, leading: false),
                        title: option.title,
                        subtitle: option.subtitle,
                        onTap: () async {
                          if (nform == null) {
                            if (onItemTap == null) {
                              Get.back(result: option);
                            } else {
                              onItemTap(option.item);
                            }
                          } else {
                            if (onItemTap == null) {
                              final result = await nform.show(
                                fullscreenDialog: true,
                                preventDuplicates: false,
                              );
                              Get.back(result: result);
                            } else {
                              onItemTap(option.item);
                            }
                          }
                        },
                      ),
                    ),
                  );
                })
                .values
                .toList(),
          )
        : Center(child: Text("message.options_form_no_item".tr));
  }
}
