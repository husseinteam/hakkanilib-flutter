import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hakkanilib/components/forms/index.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/services/index.dart';

class EditForm {
  static Widget of<TEntity extends GenericEntity<TEntity>>({
    @required TEntity entity,
    GenericEntity withOwner,
    void Function(TEntity) specializer,
  }) {
    return _Form<TEntity>(withOwner, entity, specializer);
  }
}

class _Form<TEntity extends GenericEntity<TEntity>> extends StatelessWidget {
  final TEntity entity;
  final GenericEntity ownerEntity;
  final TEntity Function(TEntity) specializer;

  _Form(this.ownerEntity, this.entity, this.specializer);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("title.edit_entity".trArgs(["${entity.tableInfo.repr}"])),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Card(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Column(
                children: _buildList(),
              ),
            ),
            margin: EdgeInsets.all(10.0),
            elevation: 3.0,
            clipBehavior: Clip.antiAlias,
          ),
        ],
      ),
    );
  }

  void _save() {
    if (specializer != null) {
      specializer(entity);
    }
    entity.owner = ownerEntity ?? entity.owner;
    DataEngine.repositoryOf<TEntity>().update(entity);
    Get.back(result: entity);
  }

  List<Widget> _buildList() {
    var fiList =
        entity.tableInfo.fieldInfos.where((fi) => fi.displayOnForm).toList();
    return List<Widget>.generate(
      fiList.length,
      (index) {
        final tile = ListTile(
          title: Text("${fiList[index].repr}"),
          subtitle: Obx(() => Text(
                "${fiList[index].prop.getter(entity)}",
                overflow: TextOverflow.ellipsis,
              )),
          trailing: Icon(Icons.arrow_forward_ios),
          onTap:
              EditField.of(fieldInfo: fiList[index], withEntity: entity).show,
        );
        return Column(
          children: <Widget>[
            tile,
            Divider(
              color: Colors.white,
            ) //                           <-- Divider
          ],
        );
      },
    )..add(
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8.0, bottom: 8.0),
                child: ElevatedButton.icon(
                  onPressed: _save,
                  icon: Icon(Icons.save),
                  label: Text('label.save'.tr),
                ),
              ),
            ],
          ),
        ),
      );
  }
}
