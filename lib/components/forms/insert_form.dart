import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hakkanilib/data/entities/index.dart';
import 'package:hakkanilib/data/metadata/index.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:hakkanilib/services/index.dart';

enum FormType {
  insert,
  edit,
}

class InsertForm {
  static Widget of<TEntity extends GenericEntity<TEntity>>(
    GlobalKey<FormState> _formKey, {
    GenericEntity withOwner,
    void Function(TEntity) specializer,
  }) {
    return _Form<TEntity>(_formKey, withOwner, specializer);
  }
}

class _Form<TEntity extends GenericEntity<TEntity>> extends StatelessWidget {
  final GenericEntity withOwner;
  final TEntity Function(TEntity) specializer;

  final TEntity _entity = DataEngine.instanceOf(TEntity);
  final GlobalKey<FormState> _formKey;

  _Form(this._formKey, [this.withOwner, this.specializer]);

  @override
  Widget build(BuildContext context) {
    List<Widget> formChildren = [
      Obx(() => Card(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 19.0, vertical: 8.0),
              child: _buildForm(context),
            ),
            margin: EdgeInsets.symmetric(horizontal: 10.0),
            elevation: 3.0,
            clipBehavior: Clip.antiAlias,
          ))
    ];
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "title.new_entity".trArgs(["${_entity.tableInfo.repr}"]),
        ),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: formChildren,
      ),
    );
  }

  Future _save() async {
    final FormState form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      if (specializer != null) {
        specializer(_entity);
      }
      _entity.owner = withOwner;
      final e = await DataEngine.repositoryOf<TEntity>().insert(_entity);
      Get.back(result: e);
    } else {
      UIUtils.alert(
        title: "alert.error_occurred".tr,
        message: "error.form_save".tr,
        multipleChoice: false,
      );
    }
  }

  Widget _buildForm(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: _buildFormInputs(
          context,
          _entity.tableInfo.fieldInfos.where((fi) => fi.displayOnForm).toList(),
        ),
      ),
    );
  }

  List<Widget> _buildFormInputs(
    BuildContext context,
    List<FieldInfo<TEntity>> fiList,
  ) {
    Tuple<FocusNode, Widget> currentTuple;
    return List<Widget>.generate(fiList.length, (index) {
      currentTuple = _buildField(
        context,
        fiList[index],
        previousNode: index > 0 ? currentTuple.it1 : null,
      );
      return currentTuple.it2;
    })
      ..add(
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              ElevatedButton.icon(
                onPressed: _save,
                icon: Icon(Icons.save),
                label: Text('label.save'.tr),
              ),
            ],
          ),
        ),
      )
      ..insert(
        0,
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                "title.new_entity".trArgs(["${_entity.tableInfo.repr}"]),
              ),
            )
          ],
        ),
      );
  }

  Tuple<FocusNode, Widget> _buildField(
      BuildContext context, FieldInfo<TEntity> fieldInfo,
      {FocusNode previousNode}) {
    var inputType = TextInputType.text;
    dynamic Function(String) transform = (input) => input;
    switch (fieldInfo.dataType) {
      case DataType.int:
        inputType = TextInputType.number;
        transform = (String input) => int.parse(input);
        break;
      case DataType.real:
        inputType = TextInputType.numberWithOptions(decimal: true);
        transform = (String input) => num.parse(input);
        break;
      default:
        break;
    }
    final text = () {
      final val = fieldInfo.prop.getter(this._entity);
      if (val is int && val == 0) {
        return "";
      }
      if (val is num && val == 0.0) {
        return "";
      }
      return val.toString();
    };
    final currentNode = previousNode ?? FocusNode();
    final nextFocus = FocusNode();
    return Tuple<FocusNode, Widget>(
      it1: nextFocus,
      it2: TextFormField(
        keyboardType: inputType,
        focusNode: currentNode,
        minLines: 1,
        maxLines: text().length ~/ 30 + 1,
        controller: TextEditingController(text: text()),
        onFieldSubmitted:
            _entity.tableInfo.fieldInfos.last.prop.name == fieldInfo.prop.name
                ? null
                : (term) {
                    currentNode.unfocus();
                    FocusScope.of(context).requestFocus(nextFocus);
                  },
        onSaved: (value) {
          if (value?.isNotEmpty == true) {
            fieldInfo.prop.setter(_entity, transform(value));
          } else if (fieldInfo.nullableOnForm &&
              fieldInfo.prop.defaultValueGetter != null) {
            fieldInfo.prop.setter(_entity, fieldInfo.prop.defaultValueGetter());
          }
        },
        decoration: InputDecoration(labelText: fieldInfo.repr),
        enableSuggestions: true,
        textInputAction:
            _entity.tableInfo.fieldInfos.last.prop.name == fieldInfo.prop.name
                ? TextInputAction.done
                : TextInputAction.next,
        //maxLength: 32,
        style: TextStyle(fontSize: 17),
        validator: (value) {
          if (value.isEmpty) {
            return fieldInfo.nullableOnForm ? null : "error.value_empty".tr;
          } else if (fieldInfo.dataType == DataType.int) {
            return value.isInt ? null : "error.not_int".tr;
          } else {
            return null;
          }
        },
      ),
    );
  }
}
