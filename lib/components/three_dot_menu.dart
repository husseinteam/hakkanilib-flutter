import 'package:flutter/material.dart';
import 'package:hakkanilib/utils/index.dart';
import 'package:get/get.dart';

enum ThreeDotMenuAction {
  edit,
  reset,
  delete,
}

extension TDMExtension on ThreeDotMenuAction {
  get string {
    switch (this) {
      case ThreeDotMenuAction.edit:
        return "tdm_edit".tr;
      case ThreeDotMenuAction.reset:
        return "tdm_reset".tr;
      case ThreeDotMenuAction.delete:
        return "tdm_delete".tr;
    }
  }

  get item {
    switch (this) {
      case ThreeDotMenuAction.edit:
        return Row(
          children: [
            Icon(
              Icons.edit,
              color: Constants.buttonColor,
            ),
            Text(" "),
            Text(string),
          ],
        );
      case ThreeDotMenuAction.reset:
        return Row(
          children: [
            Icon(
              Icons.refresh,
              color: Constants.buttonColor,
            ),
            Text(" "),
            Text(string),
          ],
        );
      case ThreeDotMenuAction.delete:
        return Row(
          children: [
            Icon(
              Icons.delete,
              color: Constants.buttonColor,
            ),
            Text(" "),
            Text(string),
          ],
        );
    }
  }
}

extension TDMListExtension on List<ThreeDotMenuAction> {
  get menuItems =>
      this.map((tdm) => PopupMenuItem(value: tdm, child: tdm.item)).toList();
}
